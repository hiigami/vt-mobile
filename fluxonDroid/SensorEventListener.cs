﻿using System;
using Android.Hardware;
namespace fluxonDroid
{
    public class SensorEventListener : Java.Lang.Object, ISensorEventListener
	{
        public delegate void SensorValueChangedDelegate(float amount);
        public event SensorValueChangedDelegate SensorValueChanged;

        public void OnSensorChanged(SensorEvent e)
		{
            if (SensorValueChanged != null)
            {
                SensorValueChanged(e.Values[0]);
            }
		}
		public void OnAccuracyChanged(Sensor sensor, SensorStatus accuracy)
		{
		}
    }
}
