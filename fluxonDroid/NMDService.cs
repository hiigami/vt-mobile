﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Android.Net;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Telephony;
using Newtonsoft.Json.Linq;
using Android.Hardware;

using Android.Locations;

using fluxon.API;

namespace fluxonDroid
{
    [Service]
    public class NMDService : Service
    {
        readonly int DELAY_BETWEEN_CALLS = 300000; // 5 minutes - handler1
        readonly int DELAY_BETWEEN_CALLS2 = 1000; // 1 second - GetSpeedAndHeading 
        readonly int MAX_STRENGTH = 100; // signal
        readonly int MIN_STRENGTH = -1; // signal
        readonly int MAX_TEMP = 70;
        readonly int MIN_TEMP = -90;

        readonly double SCORE_MAX = Math.Pow(10, 12);
        readonly double SCORE_MIN = Math.Pow(10, 12) * -1;

        readonly string TAG = typeof(NMDService).FullName;

        readonly ConnectivityType network_type = ConnectivityType.Mobile;

        bool isStarted;
        bool tempSensor = true;
        int _strength;
        float _amount;


        TelephonyManager _telephonyManager;
        GsmSignalStrengthListener _signalStrengthListener;

        SensorManager _sensorManager;
        SensorEventListener _ambienteTemperatureListener;

        Handler handler1;
        Action runnable1;

        CancellationTokenSource _cts;

        public override void OnCreate()
        {
            base.OnCreate();

            _cts = new CancellationTokenSource();

            _telephonyManager = (TelephonyManager)GetSystemService(Context.TelephonyService);
            _sensorManager = (SensorManager)GetSystemService(SensorService);
            _signalStrengthListener = new GsmSignalStrengthListener();
            _ambienteTemperatureListener = new SensorEventListener();

            handler1 = new Handler();

            runnable1 = new Action(() =>
            {
                GetTemperature();
                GetNetworkInfo();
                GetSpeedAndHeading();
                handler1.PostDelayed(runnable1, DELAY_BETWEEN_CALLS);
            });
        }

        public override StartCommandResult OnStartCommand(Android.Content.Intent intent, StartCommandFlags flags, int startId)
        {
            if (isStarted)
            {
                Log.Debug(TAG, "OnStartCommand: This service has already been started.");
            }
            else
            {
                Log.Debug(TAG, "OnStartCommand: The service is starting.");

                handler1.PostDelayed(runnable1, DELAY_BETWEEN_CALLS);

                isStarted = true;
            }
            return StartCommandResult.Sticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnDestroy()
        {
            Log.Info(TAG, "OnDestroy: The started service is shutting down.");
            handler1.RemoveCallbacks(runnable1);

            isStarted = false;

            base.OnDestroy();
        }

        async Task send_location_data(MyLocation[] locations)
        {
            Log.Debug(TAG, "send_location_data");

            if (locations != null)
            {
                JArray array = new JArray();
                array.Add(JObject.FromObject(new { name = "Measurement", description = "Degrees" }));
                JObject data = new JObject();
                data["traits"] = array;

                decimal speed = 0;
                Compute compute = new Compute();

                if (locations[1].HasBearing)
                {
                    await compute.add((decimal)locations[1].Bearing,
                                      barcodes.direction,
                                      _cts.Token,
                                      locations[1],
                                      data);
                }
                try
                {
                    if (locations[1].HasSpeed && locations[1].HasAccuracy && locations[1].Accuracy < 80)
                    {
                        speed = (decimal)((locations[1].Speed * 1.0) * 3.6d);
                    }
                    else
                    {
                        double d1 = GeoHandler.distance(locations[1].Latitude,
                                                        locations[1].Longitude,
                                                        locations[0].Latitude,
                                                        locations[0].Longitude);

                        long t1 = locations[1].Time - locations[0].Time;
                        speed = (decimal)(d1 / t1);
                    }
                    array = new JArray();
                    array.Add(JObject.FromObject(new { name = "Measurement", description = "km/h" }));
                    data = new JObject();
                    data["traits"] = array;

                    Log.Info(TAG, String.Format("speed is {0}", speed));

                    await compute.add(Decimal.Round(speed, 6),
                                      barcodes.speed,
                                      _cts.Token,
                                      locations[1],
                                      data);
                }
                catch (Exception ex)
                {
                    Log.Error(TAG, ex.Message);
                }
            }
        }

        void GetSpeedAndHeading()
        {
            try
            {
                MyLocation[] mylocations = new MyLocation[2];
                GeoHandler geoHandler = new GeoHandler();

                geoHandler.locationCriteria.BearingRequired = true;
                geoHandler.locationCriteria.SpeedRequired = true;
                geoHandler.locationCriteria.SpeedAccuracy = Accuracy.Fine;
                geoHandler.locationCriteria.BearingAccuracy = Accuracy.Fine;

                geoHandler.Error += (sender1, e1) =>
                {
                    throw new Exception("GeoHandler max retries.");
                };

                geoHandler.Completed += async (sender, e) =>
                {
                    mylocations[0] = new MyLocation(geoHandler.locations[0]);
                    mylocations[1] = new MyLocation(geoHandler.locations[1]);

                    await send_location_data(mylocations);
                };

                geoHandler.Start(DELAY_BETWEEN_CALLS2);

            }
            catch (Exception ex)
            {
                Log.Error(TAG, ex.Message);
            }
        }

        void GetTemperature()
        {
            if (tempSensor)
            {
                try
                {
                    Sensor ambientTemperature = _sensorManager.GetDefaultSensor(SensorType.AmbientTemperature);
                    if (ambientTemperature != null)
                    {
                        GeoHandler geoHandler = new GeoHandler();

                        geoHandler.Error += (sender1, e1) =>
                        {
                            throw new Exception("GeoHandler max retries.");
                        };

                        geoHandler.Completed += async (sender, e) =>
                        {
                            _sensorManager.RegisterListener(_ambienteTemperatureListener, ambientTemperature, SensorDelay.Fastest);
                            _ambienteTemperatureListener.SensorValueChanged += HandleSensorValueChanged;
                            decimal score = (decimal)(
                                ((_amount - MAX_TEMP) * (SCORE_MIN - SCORE_MAX) / (MIN_TEMP - MAX_TEMP)) + SCORE_MAX
                            );

                            JArray array = new JArray();
                            array.Add(JObject.FromObject(new { name = "Measurement", description = "Celsius" }));
                            JObject data = new JObject();
                            data["traits"] = array;

                            Compute compute = new Compute();
                            await compute.add(score,
                                              barcodes.temperature,
                                              _cts.Token,
                                              new MyLocation(geoHandler.locations[0]),
                                              data);
                        };
                        geoHandler.Start();
                    }
                    else
                    {
                        tempSensor = false;
                        _sensorManager.Dispose();
                        _ambienteTemperatureListener.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(TAG, ex.Message);
                }
            }
        }

        NetworkInfo GetActiveNetworkInfo()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);
            return connectivityManager.ActiveNetworkInfo;
        }

        void GetNetworkInfo()
        {
            try
            {
                JArray array;
                JObject data;

                GeoHandler geoHandler = new GeoHandler(1);

                geoHandler.Error += (sender1, e1) =>
                {
                    throw new Exception("GeoHandler max retries.");
                };

                geoHandler.Completed += async (sender, e) =>
                {
                    NetworkInfo info = GetActiveNetworkInfo();
                    Compute compute = new Compute();

                    if (info.Type == network_type)
                    {
                        // Log.Debug(TAG, info.SubtypeName);
                        // Log.Debug(TAG, info.ExtraInfo);

                        _telephonyManager.Listen(_signalStrengthListener, PhoneStateListenerFlags.SignalStrengths);
                        _signalStrengthListener.SignalStrengthChanged += HandleSignalStrengthChanged;
                        decimal score = (decimal)(
                            ((_strength - MAX_STRENGTH) * (SCORE_MIN - SCORE_MAX) / (MIN_STRENGTH - MAX_STRENGTH)) + SCORE_MAX
                        );
                        if (info == null) {
                            info = GetActiveNetworkInfo();
                        }
                        array = new JArray();
                        array.Add(JObject.FromObject(new { name = "Technology", description = info.SubtypeName }));
                        array.Add(JObject.FromObject(new { name = "MNO", description = info.ExtraInfo }));
                        array.Add(JObject.FromObject(new { name = "Device", description = Android.OS.Build.Model }));
                        if (_telephonyManager.IsNetworkRoaming)
                        {
                            array.Add(JObject.FromObject(new { name = "Other", description = "Roaming" }));
                        }
                        data = new JObject();
                        data["traits"] = array;

                        await compute.add(score,
                                          barcodes.network,
                                          _cts.Token,
                                          new MyLocation(geoHandler.locations[0]),
                                          data);

                    }
                    else if (info.Type == ConnectivityType.Wifi)
                    {
                        Android.Net.Wifi.WifiManager a = (Android.Net.Wifi.WifiManager)GetSystemService(WifiService);

                        array = new JArray();
                        array.Add(JObject.FromObject(new { name = "BSSID", description = a.ConnectionInfo.BSSID }));

                        decimal level = 1;
                        decimal link_speed = a.ConnectionInfo.LinkSpeed;

                        foreach (var x in a.ScanResults)
                        {
                            if (a.ConnectionInfo.BSSID.Contains(x.Bssid))
                            {
                                level = x.Level;
                                try
                                {
                                    if (x.OperatorFriendlyName.Length() > 0)
                                    {
                                        array.Add(JObject.FromObject(new
                                        {
                                            name = "OFN",
                                            description = x.OperatorFriendlyName.ToString()
                                        }));
                                    }

                                    if (x.VenueName.Length() > 0)
                                    {
                                        array.Add(JObject.FromObject(new
                                        {
                                            name = "VenueName",
                                            description = x.VenueName.ToString()
                                        }));
                                    }
                                    array.Add(JObject.FromObject(new
                                    {
                                        name = "ChannelWidth",
                                        description = Enum.GetName(typeof(Android.Net.Wifi.WifiChannelWidth), x.ChannelWidth)
                                    }));
                                }
                                catch (Exception ex)
                                {
                                    Log.Error(TAG, ex.Message);
                                }

                                var r = new Regex(@"\[(.*?)\]");
                                MatchCollection capabilities = r.Matches(x.Capabilities);
                                foreach (Match item in capabilities)
                                {
                                    array.Add(JObject.FromObject(new
                                    {
                                        name = "Capabilities",
                                        description = item.Groups[1].Value
                                    }));
                                }
                                break;
                            }
                        }

                        array.Add(JObject.FromObject(new { name = "Measurement", description = "dBm" }));
                        data = new JObject();
                        data["traits"] = array;

                        if (level < 1)
                        {
                            await compute.add(Decimal.Round(level, 4),
                                              barcodes.wifi_level,
                                              _cts.Token,
                                              new MyLocation(geoHandler.locations[0]),
                                              data);
                        }

                        //array.Last.Remove();
                        //array.Add(JObject.FromObject(new { name = "Measurement", description = Android.Net.Wifi.WifiInfo.LinkSpeedUnits }));
                        //data = new JObject();
                        //data["traits"] = array;
                        //response = await compute.add(link_speed, barcodes.wifi_link_speed, _cts.Token, data);
                        //Log.Debug(TAG, response.statusCode.ToString());
                    }
                };
                geoHandler.Start();
            }
            catch (Exception ex)
            {
                Log.Error(TAG, ex.Message);
            }
        }

        void HandleSignalStrengthChanged(int strength)
        {
            _signalStrengthListener.SignalStrengthChanged -= HandleSignalStrengthChanged;
            _telephonyManager.Listen(_signalStrengthListener, PhoneStateListenerFlags.None);
            _strength = strength;
        }

        void HandleSensorValueChanged(float amount)
        {
            _ambienteTemperatureListener.SensorValueChanged -= HandleSensorValueChanged;
            _sensorManager.UnregisterListener(_ambienteTemperatureListener);
            _amount = amount;
            Log.Debug(TAG, "TEMP ---->>>> " + amount.ToString());
        }
    }
}
