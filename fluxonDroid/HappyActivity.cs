﻿using System;
using System.Threading;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Support.V7.App;
using Android.Widget;
using Android.Content;
using fluxon.Models;
using fluxon.API;
using fluxon.Data;

namespace fluxonDroid
{
    [Activity(Theme = "@style/Theme.Base", NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
              ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation |
              Android.Content.PM.ConfigChanges.ScreenSize |
              Android.Content.PM.ConfigChanges.KeyboardHidden |
              Android.Content.PM.ConfigChanges.Keyboard)]
    public class HappyActivity : AppCompatActivity
    {
        int _active = 2;
        int _progress = 50;
        string barcode = "";
        double SCORE_MAX = Math.Pow(10, 12);
        double SCORE_MIN = Math.Pow(10, 12) * -1;
        System.Timers.Timer _timer;
        GeoHandler geoHandler = new GeoHandler(1);

        ProgressBar progressBar;

        CancellationTokenSource _cts;

        int[] slider_images = {
            Resource.Id.feellings_img_1,
            Resource.Id.feellings_img_2,
            Resource.Id.feellings_img_3,
            Resource.Id.feellings_img_4,
            Resource.Id.feellings_img_5
        };
        Dictionary<string, int[]> dictionary = new Dictionary<string, int[]>() {
            {barcodes.happy, new int[]{
                    Resource.Mipmap.happy_icon,
                    Resource.Mipmap.happy_very_green,
                    Resource.Mipmap.happy_green,
                    Resource.Mipmap.happy_yellow,
                    Resource.Mipmap.happy_orange,
                    Resource.Mipmap.happy_red,
                    Resource.String.happy_question
                }},
            {barcodes.safety, new int[]{
                    Resource.Mipmap.safety_icon,
                    Resource.Mipmap.safety_very_green,
                    Resource.Mipmap.safety_green,
                    Resource.Mipmap.safety_yellow,
                    Resource.Mipmap.safety_orange,
                    Resource.Mipmap.safety_red,
                    Resource.String.safety_question
                }},
            {barcodes.tissue, new int[]{
                    Resource.Mipmap.tissue_icon,
                    Resource.Mipmap.happy_icon,
                    Resource.Drawable.abc_list_selector_background_transition_holo_dark,
                    Resource.Drawable.abc_list_selector_background_transition_holo_dark,
                    Resource.Drawable.abc_list_selector_background_transition_holo_dark,
                    Resource.Mipmap.tissue_icon,
                    Resource.String.tissue_question
                }},
        };

        protected void set_timerItem()
        {
            TimerItem ti = new TimerItem();
            ti.barcode = barcode;
            ti.limit_minutes = 10; // TODO - change, get it from request
            ti.created = DateTime.Now;
            App.Database.raw("DELETE FROM TimerItem where barcode = ?", barcode);
            App.Database.SaveItem(ti);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            }

            SetContentView(Resource.Layout.happy);

            barcode = Intent.GetStringExtra("barcode") ?? "";
            _cts = new CancellationTokenSource();

            var all_scores = App.Database.GetItems<ScoreItem>();
            var item = all_scores.Find(x => x.barcode == barcode);
            if (item != null)
                setPoints(item);

            TextView question_txt = FindViewById<TextView>(Resource.Id.question);
            question_txt.Text = GetString(dictionary[barcode][6]);

            ImageView barcode_icon = FindViewById<ImageView>(Resource.Id.actual_felling);
            barcode_icon.SetImageResource(dictionary[barcode][0]);


            for (int i = 0; i < slider_images.Length; i++)
            {
                ImageView _slider_image_view = FindViewById<ImageView>(slider_images[i]);
                _slider_image_view.SetImageResource(dictionary[barcode][i + 1]);
                if (barcode == barcodes.tissue && i > 0 && i < slider_images.Length - 1)
                    _slider_image_view.Alpha = 0;
            }

            progressBar = FindViewById<ProgressBar>(Resource.Id.progressBar);
            progressBar.Visibility = ViewStates.Gone;

            ImageButton _home = FindViewById<ImageButton>(Resource.Id.happy_home_btn);
            ImageButton _map = FindViewById<ImageButton>(Resource.Id.happy_map_btn);
            _home.Click += (sender, e) =>
            {
                var intent = new Intent(Application.Context, typeof(LauncherActivity));
                intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                //intent.SetFlags(ActivityFlags.ReorderToFront);
                StartActivity(intent);
                Finish();
            };

            _map.Click += (sender, e) =>
            {
                try
                {
                    if (!network.isOnline())
                    {
                        Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                        builder.SetTitle(GetString(Resource.String.offline_txt_title));
                        builder.SetMessage(GetString(Resource.String.offline_txt));
                        builder.SetCancelable(true);
                        builder.SetPositiveButton(GetString(Resource.String.ok),
                                                  delegate
                                                  {
                                                      builder.Dispose();
                                                  });
                        builder.Show();
                    }
                    else if (geoHandler.is_enabled())
                    {
                        progressBar.Visibility = ViewStates.Visible;
                        _timer = new System.Timers.Timer(80);
                        _timer.Elapsed += RunUpdateLoop;
                        _timer.Start();

                        decimal score = (decimal)((_progress - 0) * ((SCORE_MIN - SCORE_MAX) * 0.01) + SCORE_MAX);

                        geoHandler.Error += (sender1, e1) =>
                        {
                            if (!_cts.Token.IsCancellationRequested)
                            {
                                clearProgress();
                                Toast.MakeText(this,
                                               "The maximum geolocation attempt number was reached",
                                               ToastLength.Long).Show();
                            }
                        };

                        geoHandler.Completed += async (sender1, e1) =>
                        {
                            Compute compute = new Compute();
                            var response = await compute.add(score,
                                                             barcode,
                                                             _cts.Token,
                                                             new MyLocation(geoHandler.locations[0]));

                            _cts.Token.ThrowIfCancellationRequested();
                            if (response != null)
                            {
                                if (response.statusCode == System.Net.HttpStatusCode.Created)
                                {
                                    set_timerItem();
                                    var intent = new Intent(Application.Context, typeof(MapActivity));
                                    intent.PutExtra("barcode", barcode);
                                    intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                                    //intent.SetFlags(ActivityFlags.ReorderToFront);
                                    StartActivity(intent);
                                    Finish();
                                }
                                else if (response.isError)
                                {
                                    clearProgress();
                                    string toast = response.content;
                                    Toast.MakeText(this, toast, ToastLength.Long).Show();
                                }
                                else
                                {
                                    clearProgress();
                                    Toast.MakeText(this, "An error has ocurred!", ToastLength.Long).Show();
                                }
                            }
                            else
                            {
                                clearProgress();
                                Toast.MakeText(this, "Unavailable service!", ToastLength.Long).Show();
                            }
                        };
                        geoHandler.Start(1000);
                    }
                    else
                    {
                        Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                        builder.SetTitle(GetString(Resource.String.enable_gps_title));
                        builder.SetMessage(GetString(Resource.String.enable_gps));
                        builder.SetCancelable(true);
                        builder.SetPositiveButton(GetString(Resource.String.yes),
                                                  delegate
                                                  {
                                                      var intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                                                      StartActivity(intent);
                                                  });
                        builder.SetNegativeButton(GetString(Resource.String.no),
                                                  delegate
                                                  {
                                                      builder.Dispose();
                                                  });
                        builder.Show();
                    }
                }
                catch (Exception ex)
                {
                    if (!_cts.Token.IsCancellationRequested)
                    {
                        clearProgress();
                        Toast.MakeText(this, "An error has ocurred!", ToastLength.Long).Show();
                    }
                }
            };
            setSliderStyle();
        }

        public void setSliderStyle()
        {
            SeekBar skb = FindViewById<SeekBar>(Resource.Id.seekBar1);
            skb.Progress = 50;

            for (int i = 0; i < slider_images.Length; i++)
            {
                ImageView _imageView = FindViewById<ImageView>(slider_images[i]);
                if (i == _active)
                    _imageView.ImageAlpha = 255;
                else
                    _imageView.ImageAlpha = 50;
            }
            skb.ProgressChanged += (object sender, SeekBar.ProgressChangedEventArgs e) =>
            {
                if (e.FromUser)
                {
                    if (e.Progress < 20)
                    {
                        _active = 0;
                    }
                    else if (e.Progress < 40)
                    {
                        _active = 1;
                    }
                    else if (e.Progress < 60)
                    {
                        _active = 2;
                    }
                    else if (e.Progress < 80)
                    {
                        _active = 3;
                    }
                    else
                    {
                        _active = 4;
                    }
                    for (int i = 0; i < slider_images.Length; i++)
                    {
                        ImageView _imageView = FindViewById<ImageView>(slider_images[i]);
                        if (i == _active)
                            _imageView.ImageAlpha = 255;
                        else
                            _imageView.ImageAlpha = 50;
                    }
                    _progress = e.Progress;
                }
            };
        }

        public void setPoints(ScoreItem item)
        {
            TextView points_txt = FindViewById<TextView>(Resource.Id.points_txt);
            ImageView indicator = FindViewById<ImageView>(Resource.Id.actual_arrow);
            points_txt.Text = Math.Round(item.score_update).ToString();
            if (item.change != 0)
            {
                if (item.change > 0)
                    indicator.SetImageResource(Resource.Mipmap.up_arrow);
                else
                    indicator.SetImageResource(Resource.Mipmap.down_arrow);
                indicator.Alpha = 1;
            }
        }

        public void updateTimerText()
        {
            int add_progress = 1;
            RunOnUiThread(() =>
            {
                if (progressBar.Progress >= 100)
                {
                    progressBar.Progress = 0;
                }
                progressBar.Progress += add_progress;
            });
        }

        void clearProgress()
        {
            clearTimer();
            progressBar.Visibility = ViewStates.Gone;
        }

        void clearTimer()
        {
            if (_timer != null)
            {
                try
                {
                    _timer.Stop();
                    _timer.Dispose();
                    _timer = null;
                }
                catch (Exception ex) { }
            }
        }

        protected void RunUpdateLoop(Object sender, System.Timers.ElapsedEventArgs e)
        {
            updateTimerText();
        }

        public override void OnBackPressed()
        {
            if (_cts != null)
                _cts.Cancel();
            clearTimer();
            base.OnBackPressed();
            var intent = new Intent(Application.Context, typeof(LauncherActivity));
            intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
            //intent.SetFlags(ActivityFlags.ReorderToFront);
            StartActivity(intent);
            Finish();
        }

        protected override void OnResume()
        {
            base.OnResume();
            _cts = new CancellationTokenSource();
        }

        protected override void OnPause()
        {
            if (_cts != null)
                _cts.Cancel();
            clearTimer();
            base.OnPause();
        }
    }
}
