﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using fluxon.API;
using Android.Util;

namespace fluxonDroid
{
	[BroadcastReceiver(Enabled = false, Exported = false)]
	[IntentFilter(new[] { "android.net.conn.CONNECTIVITY_CHANGE", "android.net.wifi.WIFI_STATE_CHANGED" })]
	public class NetworkListener: BroadcastReceiver
	{
		public TextView textView { get; set; }

		public override void OnReceive(Context context, Intent intent)
		{
            try
            {
                if (network.isOnline())
                {
                    textView.Visibility = Android.Views.ViewStates.Gone;
                }
                else
                {
                    textView.Visibility = Android.Views.ViewStates.Visible;
                }
			}
			catch (Exception ex)
			{
				Log.Debug("NetworkListener.OnReceive.catch", ex.Message, ex);
			}
		}
	}
}
