﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Content;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using fluxon.API;
using fluxon.Data;
using fluxon.Models;

namespace fluxonDroid
{
    [Activity(Theme = "@style/Theme.Base", NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
              ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation |
              Android.Content.PM.ConfigChanges.ScreenSize |
              Android.Content.PM.ConfigChanges.KeyboardHidden |
              Android.Content.PM.ConfigChanges.Keyboard)]
    public class MapActivity : AppCompatActivity
    {
        int time = 0;
        int min_last = 0;
        int timer_limit = 0;
        int timerItemId = 0;
        int layoutHeigth = 0;
        int min_PeekHeight;
        string barcode = "";
        System.Timers.Timer _timer;
        NetworkListener networkListener;
        MyMapReady mymap;
        BottomSheetBehavior bsb;

        void SetMuDrawable(string val)
        {
            int d = Resource.Drawable.network_indicator;
            if (barcodes.happy == val)
            {
                d = Resource.Drawable.happy_indicator;
            }
            else if (barcodes.safety == val)
            {
                d = Resource.Drawable.safety_indicator;
            }
            else if (barcodes.tissue == val)
            {
                d = Resource.Drawable.tissue_indicator;
            }
            else if (barcodes.temperature == val)
            {
                d = Resource.Drawable.temperature_indicator;
            }
            else if (barcodes.speed == val)
            {
                d = Resource.Drawable.speed_indicator;
            }
            else if (barcodes.wifi_level == val)
            {
                d = Resource.Drawable.wifi_indicator;
            }
            else if (barcodes.download_speed == val)
            {
                d = Resource.Drawable.n_download_speed;
            }
            else if (barcodes.upload_speed == val)
            {
                d = Resource.Drawable.n_upload_speed;
            }
            else if (barcodes.network_latency == val)
            {
                d = Resource.Drawable.n_latency;
            }

            ImageView mu_icon = FindViewById<ImageView>(Resource.Id.actual_felling);
            mu_icon.SetImageResource(d);
            mu_icon.SetImageLevel(1000000);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            }

            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
            Java.Lang.JavaSystem.Gc();

            SetContentView(Resource.Layout.map_fragment);

            barcode = Intent.GetStringExtra("barcode") ?? "";

            networkListener = new NetworkListener();
            networkListener.textView = FindViewById<TextView>(Resource.Id.map_no_connection_txt);
            networkListener.textView.Visibility = Android.Views.ViewStates.Gone;

            TextView sigma_text = FindViewById<TextView>(Resource.Id.map_score_txt);
            sigma_text.Text = "";

            SetMuDrawable(barcode);

            mymap = new MyMapReady();

            Switch real_time = FindViewById<Switch>(Resource.Id.real_time_switch);
            mymap.real_time = "0";
            real_time.CheckedChange += (sender, e) =>
            {
                mymap.real_time = e.IsChecked ? "1" : "0";
                mymap.makeQuery();
            };

            FragmentTransaction tx = FragmentManager.BeginTransaction();
            MapFragment mapFrag = MapFragment.NewInstance();
            //tx.Replace(Resource.Id.map, mapFrag);
            tx.Add(Resource.Id.map, mapFrag, "map");
            tx.Commit();


            set_map(mapFrag);

            ImageButton _home = FindViewById<ImageButton>(Resource.Id.map_home_btn);
            _home.Click += (sender, e) =>
            {
                mymap.CancelToken();

                var intent = new Intent(Application.Context, typeof(LauncherActivity));
                intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                //intent.SetFlags(ActivityFlags.ReorderToFront);
                StartActivity(intent);
                Finish();
            };

            LinearLayout.LayoutParams hl = (LinearLayout.LayoutParams)FindViewById<LinearLayout>(Resource.Id.map_rl1).LayoutParameters;
            layoutHeigth = (int)(hl.Weight * Resources.DisplayMetrics.HeightPixels);

            var aa = FindViewById(Resource.Id.bottom_sheet);
            bsb = BottomSheetBehavior.From(aa);
            bsb.Hideable = false;
            min_PeekHeight = layoutHeigth - 10;
            bsb.PeekHeight = min_PeekHeight;
            bsb.State = BottomSheetBehavior.StateCollapsed;

            bsb.SetBottomSheetCallback(new MyTouchHelper());

            var a = FindViewById<ListView>(Resource.Id.trait_listView);
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                a.NestedScrollingEnabled = true;
            }

            ImageButton details_tn = FindViewById<ImageButton>(Resource.Id.more_btn);
            details_tn.Click += (sender, e) =>
            {
                if (bsb.State == BottomSheetBehavior.StateExpanded)
                {
                    bsb.PeekHeight = min_PeekHeight;
                    bsb.State = BottomSheetBehavior.StateCollapsed;
                }
                else
                {
                    bsb.State = BottomSheetBehavior.StateExpanded;
                }
            };
            FloatingActionButton s1 = FindViewById<FloatingActionButton>(Resource.Id.selection_1_fabtn);
            FloatingActionButton s2 = FindViewById<FloatingActionButton>(Resource.Id.selection_2_fabtn);
            FloatingActionButton s3 = FindViewById<FloatingActionButton>(Resource.Id.selection_3_fabtn);
            if (barcode == barcodes.download_speed ||
                barcode == barcodes.upload_speed ||
                barcode == barcodes.network_latency
               )
            {
                s1.Click += (sender, e) =>
                {
                    barcode = barcodes.download_speed;
                    mymap.barcode = barcode;
                    SetMuDrawable(barcode);
                    FindViewById<TextView>(Resource.Id.map_score_txt).Text = "";
                    FindViewById<TextView>(Resource.Id.actual_points).Text = "";
                    mymap.makeQuery();
                    s1.Enabled = false;
                    s1.Alpha = 1F;
                    s2.Enabled = true;
                    s2.Alpha = 0.5F;
                    s3.Enabled = true;
                    s3.Alpha = 0.5F;
                };
                s2.Click += (sender, e) =>
                {
                    barcode = barcodes.network_latency;
                    mymap.barcode = barcode;
                    SetMuDrawable(barcode);
                    FindViewById<TextView>(Resource.Id.map_score_txt).Text = "";
                    FindViewById<TextView>(Resource.Id.actual_points).Text = "";
                    mymap.makeQuery();
                    s1.Enabled = true;
                    s1.Alpha = 0.5F;
                    s2.Enabled = false;
                    s2.Alpha = 1F;
                    s3.Enabled = true;
                    s3.Alpha = 0.5F;
                };
                s3.Click += (sender, e) =>
                {
                    barcode = barcodes.upload_speed;
                    mymap.barcode = barcode;
                    SetMuDrawable(barcode);
                    FindViewById<TextView>(Resource.Id.map_score_txt).Text = "";
                    FindViewById<TextView>(Resource.Id.actual_points).Text = "";
                    mymap.makeQuery();
                    s1.Enabled = true;
                    s1.Alpha = 0.5F;
                    s2.Enabled = true;
                    s2.Alpha = 0.5F;
                    s3.Enabled = false;
                    s3.Alpha = 1F;
                };
            }
            else
            {
                s1.Visibility = ViewStates.Gone;
                s2.Visibility = ViewStates.Gone;
                s3.Visibility = ViewStates.Gone;
            }
        }
        public void updateTimerText(int min)
        {
            RunOnUiThread(() =>
            {
                FindViewById<TextView>(Resource.Id.map_time_txt).Text = min.ToString() + " min";
            });
        }
        public bool updateHeader(string sigma_txt, int mu_icon, string mu_text, List<IMenuItemsType> items, int count)
        {
            RunOnUiThread(() =>
            {
                FindViewById<ListView>(Resource.Id.trait_listView).Adapter = new MapCardAdapter(this, items);

                FindViewById<TextView>(Resource.Id.map_score_txt).Text = sigma_txt;
                FindViewById<TextView>(Resource.Id.actual_points).Text = mu_text;
                FindViewById<ImageView>(Resource.Id.actual_felling).SetImageLevel(mu_icon);
                FindViewById<TextView>(Resource.Id.dot_count_txt).Text =
                    String.Format("{0} {1}", GetString(Resource.String.dot_count), count);

            });
            return true;
        }
        public bool updateMap(List<CircleOptions> circles)
        {
            RunOnUiThread(() =>
            {
                mymap.map.Clear();
                try
                {
                    for (int i = circles.Count - 1; i >= 0; i--)
                    {
                        mymap.map.AddCircle(circles[i]);
                    }
                }
                catch (Exception ex) { }
            });
            return true;
        }
        int getPadding()
        {
            return layoutHeigth;
        }
        public void set_map(MapFragment mapFrag)
        {
            if (mapFrag != null)
            {
                mymap.barcode = barcode;
                mymap.updateHeaders = updateHeader;
                mymap.updateMap = updateMap;
                mymap.getPadding = getPadding;
                mapFrag.GetMapAsync(mymap);
            }
        }
        public override void OnBackPressed()
        {
            base.OnBackPressed();

            mymap.CancelToken();
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
            var intent = new Intent(Application.Context, typeof(LauncherActivity));
            intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
            //intent.SetFlags(ActivityFlags.ReorderToFront);
            StartActivity(intent);
            Finish();
        }
        protected override void OnResume()
        {
            base.OnResume();

            mymap.setCancelToken();

            IntentFilter a = new IntentFilter();
            a.AddAction("android.net.conn.CONNECTIVITY_CHANGE");
            a.AddAction("android.net.wifi.WIFI_STATE_CHANGED");
            RegisterReceiver(networkListener, a);

            if (barcode != barcodes.network)
            {
                _timer = new System.Timers.Timer(1000);
                _timer.Elapsed += RunUpdateLoop;
                var item = App.Database.GetItems<TimerItem>();
                var exists = item.Find(x => x.barcode == barcode);
                if (exists != null)
                {
                    timerItemId = exists.ID;
                    TimeSpan diference = DateTime.Now - exists.created;
                    timer_limit = exists.limit_minutes - diference.Minutes;
                    _timer.Start();
                }
            }
        }
        protected void RunUpdateLoop(Object sender, System.Timers.ElapsedEventArgs e)
        {
            time++;
            int min = timer_limit - (int)(time * 0.01666667);
            if (min_last != min)
                updateTimerText(min);
            if (min <= 0)
            {
                _timer.Stop();
                _timer.Dispose();
                _timer = null;
                App.Database.DeleteItem<TimerItem>(timerItemId);

                mymap.CancelToken();

                var intent = new Intent(Application.Context, typeof(LauncherActivity));
                intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                //intent.SetFlags(ActivityFlags.ReorderToFront);
                StartActivity(intent);
                Finish();
            }
        }
        protected override void OnPause()
        {
            UnregisterReceiver(networkListener);
            mymap.CancelToken();
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }

            base.OnPause();
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();

            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
            Java.Lang.JavaSystem.Gc();
        }
    }
    public class MyTouchHelper : BottomSheetBehavior.BottomSheetCallback
    {
        public override void OnSlide(View bottomSheet, float slideOffset)
        {
            //throw new NotImplementedException();
        }

        public override void OnStateChanged(View bottomSheet, int newState)
        {
            if (newState == BottomSheetBehavior.StateExpanded)
            {
                (BottomSheetBehavior.From(bottomSheet)).PeekHeight = ((ViewGroup)bottomSheet.Parent).Height;
            }
        }
    }
}
