﻿using System;
using System.Threading.Tasks;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
// using Android.Util;
using fluxon.Models;
using fluxon.Data;
using fluxon.API;

using Newtonsoft.Json.Linq;

namespace fluxonDroid
{
    [Activity(Theme = "@style/Theme.Splash",
              MainLauncher = true,
              NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
              ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation |
              Android.Content.PM.ConfigChanges.ScreenSize |
              Android.Content.PM.ConfigChanges.KeyboardHidden |
              Android.Content.PM.ConfigChanges.Keyboard)]
    public class SplashActivity : AppCompatActivity
    {
        CancellationTokenSource _cts;
        readonly string TAG = typeof(NMDService).FullName;

        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
            ActionBar.SetDisplayShowHomeEnabled(false);
            ActionBar.SetDisplayShowTitleEnabled(false);
            ActionBar.SetDisplayShowCustomEnabled(true);
            SQLite.SQLite3.Config(SQLite.SQLite3.ConfigOption.Serialized);
        }

        protected override void OnResume()
        {
            base.OnResume();
            _cts = new CancellationTokenSource();
            Task startupWork = new Task(() => { SimulateStartup(); });
            startupWork.Start();
        }

        async void savePhone(UserItem user)
        {
            JObject _data = new JObject();
            DeviceInfo di = new DeviceInfo();
            RequestHandler requestHandler = new RequestHandler();

            try
            {
                _cts.Token.ThrowIfCancellationRequested();

                _data["imei"] = di.getDeviceId();
                _data["line"] = di.getLineNumber();
                _data["sim"] = di.getUniqueSimIdentifier();

                JsonData formData = new JsonData()
                {
                    method = "POST",
                    path = "/accounts/profile/phone/", // profile_phone
                    credentials = true,
                    data = _data
                };

                int index = requestHandler.addNewToken();

                _cts.Token.ThrowIfCancellationRequested();

                ResponseItem response = await requestHandler.call(formData, index);

                if (response != null && response.statusCode == System.Net.HttpStatusCode.Created)
                {
                    user.registered = true;
                    App.Database.SaveItem(user);
                }
            }
            catch (System.OperationCanceledException ex)
            {
                requestHandler.CancelAllTokens();
                //Log.Debug(TAG, ex.Message, ex);
            }
            catch (Exception ex)
            {
                //Log.Error(TAG, ex.Message, ex);
            }
        }

        async void SimulateStartup()
        {
            /*var a = new TokenItem();
			a.expires_in = 36000;
			a.access_token = "2TTcVTTB8HqxZEgWS8T7ultAVBuEJX";
			a.scope = "read write";
			a.token_type = "Bearer";
			a.refresh_token = "asda";
			a.limit = new System.DateTime(2017, 11, 1);
            App.Database.SaveItem(a);*/
            //fluxon.API.auth.log_out();

            await Task.Delay(1000);

            var token = App.Database.GetItems<TokenItem>();
            if (token.Count > 0)
            {
                var user = App.Database.GetItems<UserItem>();
                if (user.Count == 1 && !user[0].registered)
                {
                    savePhone(user[0]);
                }

                var intent = new Intent(Application.Context, typeof(LauncherActivity));
                intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                StartActivity(intent);
                Finish();
            }
            else
            {
                var intent = new Intent(Application.Context, typeof(LoginActivity));
                intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                StartActivity(intent);
                Finish();
            }
        }
    }
}
