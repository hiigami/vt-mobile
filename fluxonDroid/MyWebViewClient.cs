﻿using System;
using System.Threading;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using fluxon.API;

namespace fluxonDroid
{
	public class MyWebViewClient: WebViewClient, IValueCallback
	{
		string userAgent = "Mozilla/5.0 Google";
		public ProgressBar progressBar;
		public Func<bool> isSuccess;
		public Func<bool> isFailure;
        public Func<bool> showError;
		public CancellationToken ct { get; set; }

		public override WebResourceResponse ShouldInterceptRequest(WebView view, IWebResourceRequest request)
		{
			request.RequestHeaders.Remove("User-Agent");
			request.RequestHeaders.Add("User-Agent", userAgent);
			return base.ShouldInterceptRequest(view, request);
		}
		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			progressBar.Visibility = ViewStates.Visible;
			progressBar.Progress = 0;
			base.OnPageStarted(view, url, favicon);
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (url.LastIndexOf("accounts/profile", StringComparison.OrdinalIgnoreCase) > -1)
			{
				view.Alpha = 0;
				view.EvaluateJavascript("(function() { return (document.getElementsByTagName('body')[0].innerHTML); })();",
										this);	
			}
			else
			{
				progressBar.Visibility = ViewStates.Gone;
				progressBar.Progress = 100;
			}
			base.OnPageFinished(view, url);
		}
		public override void OnReceivedHttpError(WebView view, IWebResourceRequest request, WebResourceResponse errorResponse)
		{
            view.Visibility = ViewStates.Gone;
            showError();
			base.OnReceivedHttpError(view, request, errorResponse);
		}
		public void OnReceiveValue(Java.Lang.Object value)
		{
            Auth auth = new Auth();
            if (auth.logIn((string)value, ct))
			{
				isSuccess();
			}
			else
			{
				isFailure();
			}
		}
	}
}
