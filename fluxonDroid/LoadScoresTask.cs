﻿using System;
using System.Threading;
using Android.OS;

using fluxon.Models;
using fluxon.Data;

namespace fluxonDroid
{
	public class LoadScoresTask : AsyncTask<String, Java.Lang.Void, int>
	{
		CancellationTokenSource _cts;
		public Func<ScoreItem, bool> success;

		public LoadScoresTask()
		{
			_cts = new CancellationTokenSource();
		}
		public void CancelToken()
		{
			_cts.Cancel();
		}
		protected override int RunInBackground(params string[] @params)
		{
			var all_scores = App.Database.GetItems<ScoreItem>();
			foreach (var score in all_scores)
			{
				success(score);
			}
			return 1;
		}
		protected override void OnPostExecute(int result)
		{
			if (IsCancelled)
				result = 0;
			base.OnPostExecute(result);
		}
		protected override void OnCancelled()
		{
			if (_cts != null)
				_cts.Cancel();
			base.OnCancelled();
		}
	}
}
