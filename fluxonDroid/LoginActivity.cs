﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;	
using Android.Widget;
using Android.Views;
using Android.Content;
using fluxon.API;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text;
using fluxon.Models;
using System.Threading;

namespace fluxonDroid
{
    [Activity(Theme = "@style/Theme.Base", NoHistory = true,
              ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
              ConfigurationChanges = Android.Content.PM.ConfigChanges.Locale |
              Android.Content.PM.ConfigChanges.ScreenSize)]
    public class LoginActivity : AppCompatActivity
    {
        readonly string signup_path = "/accounts/sign_up/";
        CancellationTokenSource _cts;

        private async System.Threading.Tasks.Task<bool> sing_up()
        {

            DeviceInfo di = new DeviceInfo();
            string id = di.getDeviceId();
            var a = MD5.Create();
            byte[] _hash = a.ComputeHash(Encoding.UTF8.GetBytes(id));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < _hash.Length; i++)
            {
                sBuilder.Append(_hash[i].ToString("x2"));
            }

            JObject _data = new JObject();
            _data["username"] = id;
            _data["password"] = sBuilder.ToString();
            //data["email"] = id + "@myegregor.com";

            JsonData formData = new JsonData()
            {
                method = "POST",
                path = signup_path,
                credentials = false,
                data = _data
            };

            _cts = new CancellationTokenSource();
            RequestHandler requestHandler = new RequestHandler();

            try
            {
                int index = requestHandler.addNewToken();

                _cts.Token.ThrowIfCancellationRequested();

                ResponseItem response = await requestHandler.call(formData, index);

                Auth auth = new Auth();

                _cts.Token.ThrowIfCancellationRequested();

                if (auth.signIn(response.content))
                {
                    bool resp2 = await auth.log_in(id, sBuilder.ToString(), _cts.Token);
                    if (resp2)
                    {
                        var intent = new Intent(Application.Context, typeof(LauncherActivity));
                        StartActivity(intent);
                        Finish();
                    }
                    else
                    {
                        Toast.MakeText(this, "Bad credentials!", ToastLength.Long).Show();
                    }
                }
                else
                {
                    _cts = null;
                    Toast.MakeText(this, "Server error!", ToastLength.Long).Show();
                }

                return true;
            }
            catch (System.OperationCanceledException ex)
            {
                requestHandler.CancelAllTokens();
            }

            return false;
        }

        void goToLogin(string provider)
        {
            if (network.isOnline())
            {
                if (provider != "device")
                {
                    var intent = new Intent(Application.Context, typeof(WebViewActivity));
                    intent.PutExtra("login", provider);
                    StartActivity(intent);
                    Finish();
                }
                else
                {
                    sing_up();
                }
            }
            else
            {
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                builder.SetTitle(GetString(Resource.String.offline_txt_title));
                builder.SetMessage(GetString(Resource.String.offline_txt));
                builder.SetCancelable(true);
                builder.SetPositiveButton(GetString(Resource.String.ok),
                                          delegate
                                          {
                                              builder.Dispose();
                                          });
                builder.Show();
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            }

            SetContentView(Resource.Layout.Login);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                ImageButton _fb = FindViewById<ImageButton>(Resource.Id.fb_btn);
                ImageButton _gplus = FindViewById<ImageButton>(Resource.Id.gplus_btn);
                _fb.Click += (sender, e) =>
                {
                    goToLogin("facebook");
                };
                _gplus.Click += (sender, e) =>
                {
                    goToLogin("google-oauth2");
                };
            }
            else
            {
                RelativeLayout _fb = FindViewById<RelativeLayout>(Resource.Id.happy_rl1);
                RelativeLayout _gplus = FindViewById<RelativeLayout>(Resource.Id.happy_rl2);
                _fb.Visibility = ViewStates.Gone;
                _gplus.Visibility = ViewStates.Gone;
            }
            ImageButton _device = FindViewById<ImageButton>(Resource.Id.device_btn);
            _device.Click += (sender, e) =>
            {
                goToLogin("device");
            };
            TextView _terms = FindViewById<TextView>(Resource.Id.terms_txt);
            _terms.Click += (sender, e) =>
            {
                var intent = new Intent(Application.Context, typeof(TermsActivity));
                StartActivity(intent);
                Finish();
            };
        }

        public override void OnBackPressed()
        {
            //base.OnBackPressed();
            if (_cts != null)
            {
                _cts.Cancel();
                return;
            }
            else
            {
                base.OnBackPressed();
            }
        }

        protected override void OnPause()
        {
            if (_cts != null)
                _cts.Cancel();
            base.OnPause();
        }
    }
}
