﻿using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using fluxon.API;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System;
using Android.Util;
using fluxon.Models;

namespace fluxonDroid
{
    public class MyMapReady : Java.Lang.Object, IOnMapReadyCallback
    {
        public string barcode { get; set; }
        public string real_time { get; set; }
        public GoogleMap map { get; set; }
        public Func<int> getPadding;
        public Func<string, int, string, List<IMenuItemsType>, int, bool> updateHeaders;
        public Func<List<CircleOptions>, bool> updateMap;

        bool cameraSet = false;
        readonly string compute_path = "/vt/compute/";
        CancellationTokenSource _cts;
        List<IMenuItemsType> traits = new List<IMenuItemsType>();
        RequestHandler requestHandler = new RequestHandler();

        void setCamera(LatLng position)
        {
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(position);
            builder.Zoom(16);
            builder.Tilt(0);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            map.MoveCamera(cameraUpdate);
        }
        protected CircleOptions create_circle(double lat,
                                              double lng,
                                              double radius,
                                              int alpha,
                                              int red,
                                              int green,
                                              int blue)
        {
            CircleOptions circleOptions = new CircleOptions();
            circleOptions.InvokeCenter(new LatLng(lat, lng));
            if (radius < 1)
            {
                radius = 1;
            }
            circleOptions.InvokeRadius(radius);
            //circleOptions.InvokeStrokeWidth(1);
            circleOptions.InvokeStrokeColor(Color.FromArgb(255, red, green, blue).ToArgb());
            circleOptions.InvokeFillColor(Color.FromArgb(alpha, red, green, blue).ToArgb());

            return circleOptions;
        }
        string getSigmaTxt(int opt)
        {
            string res = "";
            switch (opt)
            {
                case 5:
                    res = "Super sure";
                    break;
                case 4:
                    res = "Very sure";
                    break;
                case 3:
                    res = "Sure";
                    break;
                case 2:
                    res = "Unsure";
                    break;
                case 1:
                    res = "Very unsure";
                    break;

            }
            return res;
        }
        void addTraits(JArray item)
        {
            foreach (var y in item)
            {
                var trait = traits.Find(x => x.GetMenuItemsType() == MenuTypes.TYPE_ITEM &&
                           ((MenuHeaderItem)x).HeaderText == y["name"].ToString());
                if (trait != null)
                {
                    var description = traits.Find(x => x.GetMenuItemsType() == MenuTypes.TYPE_SEPORATOR &&
                                                  ((MenuContentItem)x).Title == y["description"].ToString());
                    if (description == null)
                    {
                        traits.Insert(traits.IndexOf(trait) + 1, new MenuContentItem(y["description"].ToString()));
                    }
                }
                else
                {
                    traits.Add(new MenuHeaderItem(y["name"].ToString()));
                    traits.Add(new MenuContentItem(y["description"].ToString()));
                }
            }
        }

        void createCircle(int i, JObject o, List<CircleOptions> circles)
        {
            _cts.Token.ThrowIfCancellationRequested();
            if ((double)o["prec_radius_array"][i] < 1000)
            {
                lock (circles)
                {
                    //Log.Info("---->>>>", o["mu_array"][i].ToString());
                    circles.Add(create_circle((double)o["lat_array"][i],
                                                  (double)o["long_array"][i],
                                                  (double)o["prec_radius_array"][i],
                                                  (int)o["color_array"][i]["alpha"],
                                                  (int)o["color_array"][i]["red"],
                                                  (int)o["color_array"][i]["green"],
                                                  (int)o["color_array"][i]["blue"]
                                                 ));
                }
            }
        }

        bool renderResult(ResponseItem response)
        {
            int mu_icon = 1000;
            int count = 0;
            string mu_text = "N/A";
            string sigma_txt = getSigmaTxt(0);
            traits.Clear();
            if (response != null && response.statusCode == System.Net.HttpStatusCode.OK)
            {
                try
                {
                    _cts.Token.ThrowIfCancellationRequested();
                    JObject o = JObject.Parse(response.content);
                    count = ((JArray)o["lat_array"]).Count;
                    if (count > 0)
                    {
                        sigma_txt = getSigmaTxt((int)o["global"]["mu_sigma"][1]);
                        mu_icon = Math.Abs((int)Math.Ceiling((double)o["global"]["mu_sigma"][0]));
                        if (mu_icon >= 1000000)
                        {
                            mu_text = "N/A";
                        }
                        else if (barcode == barcodes.speed)
                        {
                            mu_text = ((int)o["global"]["mu_sigma"][0]).ToString() + " km/h";
                        }
                        else if (barcode == barcodes.wifi_level)
                        {
                            mu_text = ((int)o["global"]["mu_sigma"][0]).ToString() + " dBm";
                        }
                        else if (barcode == barcodes.network_latency)
                        {
                            mu_text = ((float)o["global"]["mu_sigma"][0]).ToString() + " ms";
                        }
                        else if (barcode == barcodes.download_speed || barcode == barcodes.upload_speed)
                        {
                            mu_text = ((int)o["global"]["mu_sigma"][0]).ToString() + " Kbps";
                        }
                        else
                        {
                            mu_text = o["global"]["mu_sigma"][0].ToString() + "/5";
                        }
                        _cts.Token.ThrowIfCancellationRequested();
                        var circles = new List<CircleOptions>();
                        //for (int i = count - 1; i >= 0; i--)
                        if (count < 10)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                createCircle(i, o, circles);
                            }
                        }
                        else
                        {
                            Parallel.For(0, (count - 1), new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
                            {
                                createCircle(i, o, circles);
                            });
                        }
                        for (int i = count - 1; i >= 0; i--)
                        {
                            addTraits((JArray)o["traits_array"][i]);
                        }
                        _cts.Token.ThrowIfCancellationRequested();
                        updateMap(circles);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("map.CameraChange", ex.Message, ex);
                }
            }
            try
            {
                _cts.Token.ThrowIfCancellationRequested();
                updateHeaders(sigma_txt, mu_icon, mu_text, traits, count);
            }
            catch (System.OperationCanceledException ex)
            {
                // Log.Debug("map.CameraChange.cancel", ex.Message, ex);
            }
            return true;
        }
        protected async Task query(LatLngBounds bounds)
        {
            try
            {
                _cts.Token.ThrowIfCancellationRequested();
                requestHandler.CancelAllTokens();
                FormData aaa = new FormData()
                {
                    method = "GET",
                    data = new string[,] {
                        { "barcode",  barcode },
                        { "lat1", bounds.Southwest.Latitude.ToString().Replace(",", ".") },
                        { "lng1", bounds.Northeast.Longitude.ToString().Replace(",", ".") },
                        { "lat2", bounds.Northeast.Latitude.ToString().Replace(",", ".") },
                        { "lng2", bounds.Northeast.Longitude.ToString().Replace(",", ".") },
                        { "lat3", bounds.Northeast.Latitude.ToString().Replace(",", ".") },
                        { "lng3", bounds.Southwest.Longitude.ToString().Replace(",", ".") },
                        { "lat4", bounds.Southwest.Latitude.ToString().Replace(",", ".") },
                        { "lng4", bounds.Southwest.Longitude.ToString().Replace(",", ".") },
                        { "realtime", real_time }
                    },
                    path = compute_path,
                    credentials = true
                };

                _cts.Token.ThrowIfCancellationRequested();

                int index = requestHandler.addNewToken();
                var result = await requestHandler.call(aaa, index);

                _cts.Token.ThrowIfCancellationRequested();

                renderResult(result);
            }
            catch (System.OperationCanceledException ex)
            {
                requestHandler.CancelAllTokens();
                // Log.Debug("map.CameraChange.cancel", ex.Message, ex);
            }
        }
        public void makeQuery()
        {
            if (network.isOnline())
            {
                var bounds = map.Projection.VisibleRegion.LatLngBounds;
                query(bounds);
            }
        }
        public void OnMapReady(GoogleMap googleMap)
        {
            _cts = new CancellationTokenSource();

            map = googleMap;
            map.Clear();
            map.SetPadding(0, getPadding(), 0, getPadding());
            map.MyLocationEnabled = true;
            map.MyLocationChange += (sender, e) =>
            {
                if (!cameraSet)
                {
                    cameraSet = true;
                    setCamera(new LatLng(e.Location.Latitude, e.Location.Longitude));
                }
            };

            map.CameraChange += (sender, e) =>
            {
                makeQuery();
            };
        }
        public void setCancelToken()
        {
            if (_cts == null || _cts.IsCancellationRequested)
                _cts = new CancellationTokenSource();
        }
        public void CancelToken()
        {
            if (_cts != null)
                _cts.Cancel();
        }
    }
}
