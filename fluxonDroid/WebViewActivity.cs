﻿using System;
using System.Threading;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Android.Content;
using Android.Webkit;
using fluxon.API;

namespace fluxonDroid
{
	[Activity(Theme = "@style/Theme.Base", NoHistory = true,
			  ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
			  ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation |
			  Android.Content.PM.ConfigChanges.ScreenSize |
			  Android.Content.PM.ConfigChanges.KeyboardHidden |
			  Android.Content.PM.ConfigChanges.Keyboard)]
	public class WebViewActivity : AppCompatActivity
	{
		
		CancellationTokenSource _cts;

		public void toLauncherActivity()
		{
			var intent = new Intent(Application.Context, typeof(LauncherActivity));
			StartActivity(intent);
			Finish();
		}
		public void toLoginActivity()
		{
			var intent = new Intent(Application.Context, typeof(LoginActivity));
			StartActivity(intent);
			Finish();
		}
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.web_view);
			string app = Intent.GetStringExtra("login") ?? "";
			_cts = new CancellationTokenSource();

            FindViewById<LinearLayout>(Resource.Id.msg_500).Visibility = Android.Views.ViewStates.Gone;

			WebView localWebView = FindViewById<WebView>(Resource.Id.LocalWebView);

			if (app.LastIndexOf("google", System.StringComparison.OrdinalIgnoreCase) > -1)
				localWebView.Settings.UserAgentString = "Mozilla/5.0 Google";

			ProgressBar progressBar = FindViewById<ProgressBar>(Resource.Id.webview_progressBar);
			MyWebViewClient _webViewClient = new MyWebViewClient();

			_webViewClient.progressBar = progressBar;
			_webViewClient.ct = _cts.Token;
            _webViewClient.showError = () => {
				RunOnUiThread(() =>
				{
                    FindViewById<LinearLayout>(Resource.Id.msg_500).Visibility = Android.Views.ViewStates.Visible;
				});
                return true;
            };
			_webViewClient.isSuccess = () => {
                RunOnUiThread(() =>
                {
                    try
                    {
                        localWebView.ClearCache(true);
                        localWebView.ClearHistory();
                    }
                    catch (Exception ex)
                    {
                    }
                });
				toLauncherActivity();
				return true;
			};
			_webViewClient.isFailure = () =>
			{
				toLoginActivity();
				return true;
			};

			localWebView.SetWebViewClient(_webViewClient);
			localWebView.Settings.JavaScriptEnabled = true;
			localWebView.LoadUrl(core.base_url + "/accounts/oauth/login/" + app);
		}
		public override void OnBackPressed()
		{
			base.OnBackPressed();
			var intent = new Intent(Application.Context, typeof(LoginActivity));
            intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
			StartActivity(intent);
			Finish();
		}
		protected override void OnPause()
		{
			if (_cts != null)
				_cts.Cancel();
			base.OnPause();
		}
	}
}
