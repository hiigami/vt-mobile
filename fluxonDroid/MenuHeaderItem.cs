﻿using System;
namespace fluxonDroid
{
	public interface IMenuItemsType
	{
		int GetMenuItemsType();
	}

    public static class MenuTypes
    {
		public static int TYPE_ITEM = 0;
		public static int TYPE_SEPORATOR = 1;
    }

    public class MenuHeaderItem : IMenuItemsType
    {
        public string HeaderText { get; set; }

		public int GetMenuItemsType()
		{
			return MenuTypes.TYPE_ITEM;// return 0. It is header
		}

		public MenuHeaderItem(string _headerText)
		{
			HeaderText = _headerText;// return title of header
		}
    }
	public class MenuContentItem : IMenuItemsType
	{
		public string Title { get; set; }

		public int GetMenuItemsType()
		{
			return MenuTypes.TYPE_SEPORATOR;// return 1
		}

		public MenuContentItem(string _title)
		{
			Title = _title;
		}
	}
}
