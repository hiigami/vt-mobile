﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;

namespace fluxonDroid
{
    public class MapCardAdapter : ArrayAdapter<IMenuItemsType>
    {
		List<IMenuItemsType> items;
		Context context;
        LayoutInflater inflater;

        public MapCardAdapter(Context context, List<IMenuItemsType> items) : base(context, 0, items)
		{
			this.context = context;
			this.items = items;
            this.inflater = (LayoutInflater)this.context.GetSystemService(Context.LayoutInflaterService);
		}
		public override long GetItemId(int position)
		{
            return position;
		}
		public override int Count
		{
            get { return items.Count; }
		}
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
			View view = convertView;
            try
            {
                IMenuItemsType item = items[position];
                if (item.GetMenuItemsType() == MenuTypes.TYPE_ITEM)
                {
                    MenuHeaderItem _headerItem = (MenuHeaderItem)item;
                    view = inflater.Inflate(Resource.Layout.ListViewHeaderItem, null);
                    // user dont click header item
                    view.Clickable = false;

                    var headerName = view.FindViewById<TextView>(Resource.Id.txtHeader);
                    headerName.Text = _headerItem.HeaderText;

                }
                else if (item.GetMenuItemsType() == MenuTypes.TYPE_SEPORATOR)
                {
                    MenuContentItem _contentItem = (MenuContentItem)item;
                    view = inflater.Inflate(Resource.Layout.ListViewContentItem, null);

                    var _title = view.FindViewById<TextView>(Resource.Id.txtTitle);

                    _title.Text = _contentItem.Title;
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(context, ex.Message, ToastLength.Long);
            }
            return view;
        }
    }
}
