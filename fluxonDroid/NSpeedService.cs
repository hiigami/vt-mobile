﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Net;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Newtonsoft.Json.Linq;

using Android.Locations;

using fluxon.API;
using fluxon.SpeedTest;

namespace fluxonDroid
{
    [Service]
    public class NSpeedService : Service
    {
        bool isStarted;
        int delay_counter = 0;
        readonly int[] DELAY_BETWEEN_CALLS = new int[]{
            4980000,
            4980000,
            4980000,
            43200000,
            28260000
        }; // 83 minutes - handler1, 83 min, 12hr

        readonly string TAG = typeof(NSpeedService).FullName;

        readonly ConnectivityType network_type = ConnectivityType.Mobile;

        Handler handler1;
        Action runnable1;

        CancellationTokenSource _cts;

        public override void OnCreate()
        {
            base.OnCreate();

            _cts = new CancellationTokenSource();

            handler1 = new Handler();

            runnable1 = new Action(async () =>
            {
                await NetworkSpeedCounter();
                handler1.PostDelayed(runnable1, DELAY_BETWEEN_CALLS[delay_counter]);
            });
        }

        public override StartCommandResult OnStartCommand(Android.Content.Intent intent, StartCommandFlags flags, int startId)
        {
            if (isStarted)
            {
                Log.Debug(TAG, "OnStartCommand: This service has already been started.");
            }
            else
            {
                Log.Debug(TAG, "OnStartCommand: The service is starting.");

                handler1.PostDelayed(runnable1, DELAY_BETWEEN_CALLS[0]);

                isStarted = true;
            }
            return StartCommandResult.Sticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnDestroy()
        {
            Log.Info(TAG, "OnDestroy: The started service is shutting down.");
            handler1.RemoveCallbacks(runnable1);

            isStarted = false;

            base.OnDestroy();
        }

        async Task SendNetWorkSpeed(double score, NetworkInfo info, string barcode, MyLocation location, string measurement)
        {
            JArray array = new JArray();
            array.Add(JObject.FromObject(new { name = "Technology", description = info.SubtypeName }));
            array.Add(JObject.FromObject(new { name = "MNO", description = info.ExtraInfo }));
            array.Add(JObject.FromObject(new { name = "Device", description = Android.OS.Build.Model }));
            array.Add(JObject.FromObject(new { name = "Measurement", description = measurement }));

            if (info.IsRoaming)
            {
                array.Add(JObject.FromObject(new { name = "Other", description = "Roaming" }));
            }

            JObject data = new JObject();
            data["traits"] = array;
            Decimal new_score = Decimal.Parse((score / 1000D).ToString());

            Log.Debug(TAG, "SendNetWorkSpeed");
            Log.Debug(TAG, barcode);
            Log.Debug(TAG, Decimal.Round(new_score, 7).ToString());
            Compute compute = new Compute();
            await compute.add(Decimal.Round(new_score, 7), barcode, _cts.Token, location, data);
        }

        NetworkInfo GetActiveNetworkInfo()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);
            return connectivityManager.ActiveNetworkInfo;
        }

        async Task GetNetworkSpeed()
        {
            int count = 0;
            int face = 0;

            NetworkInfo[] info_to_send = new NetworkInfo[2];
            Location[] locations = new Location[2];

            SpeedTest speedTest = new SpeedTest();
            GeoHandler geoHandler = new GeoHandler(1, 5);

            geoHandler.Error += (sender1, e1) =>
            {
                throw new Exception("GeoHandler max retries.");
            };

            geoHandler.Completed += async (sender, e) =>
            {
                locations[count] = geoHandler.locations[0];
                count++;
                if (count == 2)
                {
                    if (face == 0)
                    {
                        count = 0;
                        face++;
                        MyLocation new_location = geoHandler.Midpoint(locations[1],
                                                                      locations[0]);
                        await SendNetWorkSpeed(speedTest.download_result,
                                               info_to_send[0],
                                               barcodes.download_speed,
                                               new_location,
                                               "Kbps");
                        await SendNetWorkSpeed((speedTest.ping_result * 1000),
                                               info_to_send[0],
                                               barcodes.network_latency,
                                               new_location,
                                               "ms");
                        geoHandler.Start();
                    }
                    else
                    {
                        MyLocation new_location = geoHandler.Midpoint(locations[1],
                                                                      locations[0]);
                        await SendNetWorkSpeed(speedTest.upload_result,
                                               info_to_send[1],
                                               barcodes.upload_speed,
                                               new_location,
                                               "Kbps");
                    }
                }
                else
                {
                    NetworkInfo info = GetActiveNetworkInfo();
                    if (face == 0)
                    {
                        if (info.Type == network_type)
                        {
                            info_to_send[0] = info;
                            await speedTest.Download();
                            geoHandler.Start();
                        }
                    }
                    else
                    {
                        if (info.Type == network_type)
                        {
                            info_to_send[1] = info;
                            await speedTest.Upload();
                            geoHandler.Start();
                        }
                    }
                }
            };

            if (GetActiveNetworkInfo().Type == network_type)
            {
                int conf = await speedTest.GetConfig();
                if (conf == 0)
                {
                    await speedTest.GetServers();
                    await speedTest.GetBestServer();
                    geoHandler.Start();
                    delay_counter = (delay_counter + 1) % 5;
                }
            }
        }

        async Task NetworkSpeedCounter()
        {
            try
            {
                NetworkInfo info = GetActiveNetworkInfo();
                if (info.Type == network_type)
                {
                    await GetNetworkSpeed();
                }
            }
            catch (Exception ex)
            {
                delay_counter = 0;
                Log.Debug(TAG, ex.Message);
            }
        }
    }
}
