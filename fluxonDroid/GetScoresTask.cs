﻿using System;
using System.Threading;
using System.Collections.Generic;
using Android.OS;
using Android.Util;

using fluxon.Models;
using fluxon.API;
using fluxon.Data;

namespace fluxonDroid
{
    public class GetScoresTask : AsyncTask<String, Java.Lang.Void, int>
    {
        readonly string score_path = "/vt/score/";
        CancellationTokenSource _cts;

        public Func<bool> failure;
        public Func<ScoreItem, bool> success;

        public GetScoresTask()
        {
            _cts = new CancellationTokenSource();
        }
        public void CancelToken()
        {
            _cts.Cancel();
        }
        protected override int RunInBackground(params string[] @params)
        {
            foreach (var para in @params)
                ask(para).Wait();
            return 1;
        }
        protected override void OnPostExecute(int result)
        {
            if (IsCancelled)
                result = 0;
            base.OnPostExecute(result);
        }
        protected override void OnCancelled()
        {
            if (_cts != null)
                _cts.Cancel();
            base.OnCancelled();
        }
        private async System.Threading.Tasks.Task<bool> ask(string barcode)
        {
            RequestHandler requestHandler = new RequestHandler();

            try
            {
                FormData formData = new FormData()
                {
                    method = "GET",
                    path = score_path,
                    credentials = true,
                    data = new string[,] { { "barcode", barcode } }
                };
                int index = requestHandler.addNewToken();

                _cts.Token.ThrowIfCancellationRequested();

                ResponseItem response = await requestHandler.call(formData, index);

                if (response != null)
                {
                    if (Auth.isAuthorized && response.statusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        _cts.Token.ThrowIfCancellationRequested();
                        failure();
                    }
                    else if (!response.isError && response.statusCode == System.Net.HttpStatusCode.OK)
                    {
                        List<ScoreItem> all_scores = core.get_score(response);
                        var item = App.Database.GetItems<ScoreItem>();
                        foreach (ScoreItem score in all_scores)
                        {
                            var exists = item.Find(x => x.barcode == score.barcode);
                            if (exists != null)
                            {
                                score.ID = exists.ID;
                            }
                            App.Database.SaveItem(score);
                            _cts.Token.ThrowIfCancellationRequested();
                            success(score);
                        }
                        return true;
                    }
                }
                _cts.Token.ThrowIfCancellationRequested();
            }
            catch (System.OperationCanceledException ex)
            {
                _cts.Cancel();
                requestHandler.CancelAllTokens();
                Log.Error("GetScoresTask.ask.cancel", ex.Message, ex);
            }
            catch (Exception ex)
            {
                _cts.Cancel();
                requestHandler.CancelAllTokens();
                Log.Error("GetScoresTask.ask", ex.Message, ex);
            }

            return false;
        }
    }
}
