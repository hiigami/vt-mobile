﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Android.Content;
using fluxon.Models;
using fluxon.API;
using fluxon.Data;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;

namespace fluxonDroid
{
	[Activity(Theme = "@style/Theme.Base", NoHistory = true,
	          ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
			  ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation |
			  Android.Content.PM.ConfigChanges.ScreenSize |
			  Android.Content.PM.ConfigChanges.KeyboardHidden |
              Android.Content.PM.ConfigChanges.Keyboard)]
	public class LauncherActivity : AppCompatActivity
	{
		GetScoresTask _task;
		LoadScoresTask _task2;
        Intent serviceToStart;
        Intent serviceToStart2;

		protected int[] getResources(string barcode)
		{
			int[] _resources = new int[3];
            if (barcode == barcodes.happy)
            {
                _resources[0] = Resource.Id.launcher_happy_points;
                _resources[1] = Resource.Id.launcher_happy_indicator;
                _resources[2] = Resource.Id.hapiness_btn;
            }
            else if (barcode == barcodes.wifi_level)
            {
                _resources[0] = Resource.Id.launcher_wifi_points;
                _resources[1] = Resource.Id.launcher_wifi_indicator;
                _resources[2] = Resource.Id.wifi_btn;
            }
            else if (barcode == barcodes.safety)
            {
                _resources[0] = Resource.Id.launcher_safety_points;
                _resources[1] = Resource.Id.launcher_safety_indicator;
                _resources[2] = Resource.Id.safety_btn;
            }
            else if (barcode == barcodes.tissue)
            {
                _resources[0] = Resource.Id.launcher_tissue_points;
                _resources[1] = Resource.Id.launcher_tissue_indicator;
                _resources[2] = Resource.Id.tissue_btn;
            }
            else if (barcode == barcodes.network)
            {
                _resources[0] = Resource.Id.launcher_network_points;
                _resources[1] = Resource.Id.launcher_network_indicator;
                _resources[2] = Resource.Id.network_btn;
            }
            else if (barcode == barcodes.speed)
            {
                _resources[0] = Resource.Id.launcher_speed_points;
                _resources[1] = Resource.Id.launcher_speed_indicator;
                _resources[2] = Resource.Id.speed_btn;
            }
            else if (barcode == barcodes.download_speed)
            {
                _resources[0] = Resource.Id.launcher_n_speed_points;
                _resources[1] = Resource.Id.launcher_n_speed_indicator;
                _resources[2] = Resource.Id.n_speed_btn;
            }
			return _resources;
		}
        long toTimestamp (DateTime d) {
            return ((DateTimeOffset)d).ToUnixTimeSeconds();
        }
		public bool hasTimer(string barcode)
		{
			var item = App.Database.GetItems<TimerItem>();
			var exists = item.Find(x => x.barcode == barcode);
			if (exists != null)
			{
                int diference = (int)(toTimestamp(DateTime.Now) - toTimestamp(exists.created)) / 60;
				int timer_limit = exists.limit_minutes - diference;
				if (timer_limit > 0)
					return true;
				App.Database.DeleteItem<TimerItem>(exists.ID);
			}
			return false;
		}
		protected void setButtons(string barcode, int button_resource)
		{
            try
            {
                Type my_type = typeof(HappyActivity);
                ImageButton _button = FindViewById<ImageButton>(button_resource);
                _button.Click += (sender, e) =>
                {
                    Intent intent;
                    if (hasTimer(barcode) || barcode == barcodes.network ||
                        barcode == barcodes.temperature || barcode == barcodes.speed ||
                        barcode == barcodes.direction || barcode == barcodes.wifi_level ||
                        barcode == barcodes.download_speed)
                    {
                        intent = new Intent(Application.Context, typeof(MapActivity));
                    }
                    else
                    {
                        intent = new Intent(Application.Context, my_type);
                    }
                    intent.PutExtra("barcode", barcode);
                    intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                    //intent.SetFlags(ActivityFlags.ReorderToFront);
                    cancel_task();
                    StartActivity(intent);
                    Finish();
                };
            }
            catch (Exception ex)
            {

            }
		}
		public void setPoints(ScoreItem item, int points_txt_resource, int points_indicator_resource)
		{
            if (points_indicator_resource > 0)
            {
                TextView points_txt = FindViewById<TextView>(points_txt_resource);
                ImageView points_indicator = FindViewById<ImageView>(points_indicator_resource);
                points_txt.Text = Math.Round(item.score_update).ToString();
                if (item.change != 0)
                {
                    if (item.change > 0)
                        points_indicator.SetImageResource(Resource.Mipmap.up_arrow);
                    else
                        points_indicator.SetImageResource(Resource.Mipmap.down_arrow);
                    points_indicator.Alpha = 1;
                }
            }
		}
		private void setPointTask(ScoreItem item, AsyncTask task, bool set_btns = false)
		{
			if (item != null)
			{
				int[] _resources = getResources(item.barcode);
				if (set_btns)
					RunOnUiThread(() => setButtons(item.barcode, _resources[2]));
				if (!task.IsCancelled)
					RunOnUiThread(() => setPoints(item, _resources[0], _resources[1]));
			}
		}
		private void cancel_task()
		{
			if (_task != null)
			{
				_task.CancelToken();
				_task.Cancel(true);
			}
		}
		private void log_out()
		{
			Auth.isAuthorized = false;
			new Auth().log_out();
			var intent = new Intent(Application.Context, typeof(LoginActivity));
			intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
			//intent.SetFlags(ActivityFlags.ReorderToFront);
			cancel_task();
			StartActivity(intent);
			Finish();
		}
		protected override void OnResume()
		{
			base.OnResume();

			CrashManager.Register(this);
			MetricsManager.Register(Application);

			Auth.isAuthorized = true;

			if (network.isOnline())
			{
				_task = new GetScoresTask();
				_task.failure = () =>
			 	{
				 	log_out();
				 	return true;
			 	};
				_task.success = (ScoreItem item) =>
				{
                    setPointTask(item, _task, true);
					return true;
				};
				_task.Execute("-1");
			}
		}
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
			{
				Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
			}

            core.user_agent = string.Format("RestSharp/105 (Linux; U; Android {0}; {1}) RestSharp/105.2.3",
                                            Build.VERSION.Release,
                                            Android.OS.Build.Model);

			SetContentView(Resource.Layout.Launcher);


			Auth.isAuthorized = true;

			_task2 = new LoadScoresTask();
			_task2.success = (ScoreItem item) =>
										{
											setPointTask(item, _task2, true);
											return true;
										};
			_task2.Execute("-1");
 
			serviceToStart = new Intent(this, typeof(NMDService));
			StartService(serviceToStart);

            serviceToStart2 = new Intent(this, typeof(NSpeedService));
            StartService(serviceToStart2);

            FindViewById<LinearLayout>(Resource.Id.rl_4_1).Visibility = ViewStates.Invisible;
		}
		public override void OnBackPressed()
		{
            base.OnBackPressed();
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
		}
		protected override void OnPause()
		{
            base.OnPause();
			cancel_task();
		}
	}
}
