﻿using Android.App;
using Android.Content;
using Android.OS;

namespace fluxonDroid
{
	[Activity(Theme = "@style/Theme.Base", NoHistory = true,
			  ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait,
			  ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation |
			  Android.Content.PM.ConfigChanges.ScreenSize |
			  Android.Content.PM.ConfigChanges.KeyboardHidden |
			  Android.Content.PM.ConfigChanges.Keyboard)]
    public class TermsActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.terms);
        }
		public override void OnBackPressed()
		{
			base.OnBackPressed();
            var intent = new Intent(Application.Context, typeof(LoginActivity));
			intent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
			StartActivity(intent);
			Finish();
		}
    }
}
