﻿using Android.Net;
using Android.App;

namespace fluxon.API
{
    public class network
    {
        public network()
        {
        }
        public static bool isOnline()
        {
            ConnectivityManager connectivityManager = (ConnectivityManager)Application.Context.ApplicationContext.GetSystemService(Android.Content.Context.ConnectivityService);
            NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;
            if (networkInfo != null)
                return networkInfo.IsConnected;
            return false;
        }
    }
}
