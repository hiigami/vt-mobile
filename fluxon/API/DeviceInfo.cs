﻿using Android.Telephony;
using Android.App;

namespace fluxon.API
{
    public class DeviceInfo
    {
        TelephonyManager telephonyManager;
        public DeviceInfo()
        {
            telephonyManager = (TelephonyManager)Application.Context.ApplicationContext.GetSystemService(Android.Content.Context.TelephonyService);
        }
        public string getDeviceId()
        {
            return telephonyManager.DeviceId;
        }
        public string getLineNumber()
        {
            return telephonyManager.Line1Number ?? "";
        }
        public string getUniqueSimIdentifier()
        {
            return telephonyManager.SimOperator + "-" + telephonyManager.SimSerialNumber;
        }
    }
}
