﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.Util;

using RestSharp;
using Newtonsoft.Json.Linq;
using fluxon.Models;

namespace fluxon.API
{
    public class RequestItem
    {
        public string method { get; set; }
        public string path { get; set; }
        public bool credentials { get; set; }
        public bool is_json = false;

        public virtual T parameters<T>()
        {
            throw new NotImplementedException();
        }
    }

    public class FormData : RequestItem
    {
        public string[,] data { get; set; }

        public override T parameters<T>()
        {
            return (T)Convert.ChangeType(data, typeof(T));
        }
    }

    public class JsonData : RequestItem
    {
        public JObject data { get; set; }

        public JsonData() : base()
        {
            this.is_json = true;
        }

        public override T parameters<T>()
        {
            return (T)Convert.ChangeType(data, typeof(T));
        }
    }

    public class CancellationTokenSourceItem
    {
        public int index;
        public CancellationTokenSource cts;
        public CancellationTokenSourceItem(int index)
        {
            this.index = index;
            this.cts = new CancellationTokenSource();
        }
    }

    public class RequestHandler
    {
        int index = 0;
        Auth auth = new Auth();
        List<CancellationTokenSourceItem> _cts = new List<CancellationTokenSourceItem>();
        RestClient client = new RestClient(core.base_url);

        public RequestHandler()
        {
            addNewToken();
            client.UserAgent = core.user_agent;
        }

        public void CancelAllTokens()
        {
            try
            {
                foreach (CancellationTokenSourceItem item in _cts)
                {
                    item.cts.Cancel();
                }
            }
            catch (Exception ex)
            {
                Log.Debug("RequestHandler.CancelAllTokens.catch", ex.Message, ex);
            }
        }

        void releaseTokens(CancellationTokenSourceItem ctsi)
        {
            ctsi.cts.Dispose();
            ctsi.cts = null;
            _cts.Remove(ctsi);
        }

        public int addNewToken()
        {
            index++;
            _cts.Add(new CancellationTokenSourceItem(index));
            return index;
        }

        Method get_method(string name)
        {
            if (name == "POST")
                return Method.POST;
            return Method.GET;
        }

        public async System.Threading.Tasks.Task<ResponseItem> call(RequestItem item, int ctsIndex)
        {
            CancellationTokenSourceItem ctsi = null;
            Method method = get_method(item.method);

            var request = new RestRequest(item.path, method);
            var taskCompletionSource = new TaskCompletionSource<ResponseItem>();

            try
            {
                ctsi = _cts.Find(x => x.index == ctsIndex);

                if (item.is_json)
                {
                    request.AddParameter("application/json; charset=utf-8",
                                         item.parameters<JObject>(),
                                         ParameterType.RequestBody);
                }
                else
                {
                    string[,] data = item.parameters<string[,]>();
                    if (data != null)
                    {
                        for (int i = 0; i < data.GetLength(0); i++)
                        {
                            request.AddParameter(data[i, 0], data[i, 1]);
                        }
                    }
                }

                ctsi.cts.Token.ThrowIfCancellationRequested();

                request = await auth.get_credentials(request, item.credentials, ctsi.cts.Token);

                ctsi.cts.Token.ThrowIfCancellationRequested();

                client.ExecuteAsync(request, response =>
                {
                    if (!ctsi.cts.Token.IsCancellationRequested)
                    {
                        if (response.ErrorException != null)
                        {
                            taskCompletionSource.SetResult(new ResponseItem(response.StatusCode,
                                                                            response.ErrorMessage,
                                                                            true));
                        }
                        else if (response.ResponseStatus == ResponseStatus.Completed)
                        {
                            try
                            {
                                taskCompletionSource.SetResult(new ResponseItem(response.StatusCode, response.Content));
                            }
                            catch (Exception ex)
                            {
                                taskCompletionSource.SetResult(new ResponseItem(response.StatusCode, "", true));
                            }
                        }
                        else
                        {
                            taskCompletionSource.SetResult(new ResponseItem(response.StatusCode, "", true));
                        }
                    }
                    releaseTokens(ctsi);
                });
            }
            catch (System.OperationCanceledException ex)
            {
                if (ctsi != null)
                    releaseTokens(ctsi);
                Log.Debug("RequestHandler.call.cancel", ex.Message, ex);
            }
            catch (Exception ex)
            {
                if (ctsi != null)
                {
                    ctsi.cts.Cancel();
                    releaseTokens(ctsi);
                }
                Log.Error("RequestHandler.call.error", ex.Message, ex);
            }
            return await taskCompletionSource.Task;
        }
    }
}
