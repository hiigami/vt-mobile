﻿using System.Collections.Generic;
using Newtonsoft.Json;
using fluxon.Models;
using System.Threading;
using System.Threading.Tasks;

namespace fluxon.API
{
    public class core
    {
        public core()
        {
        }

        public static readonly string base_url = "http://myegregor.com";
        public static string user_agent = "";

        public static async Task<UserItem> get_user(CancellationToken ct)
        {
            var user = Data.App.Database.GetItems<UserItem>();
            if (user.Count > 0)
                return user[0];

            RequestHandler requestHandler = new RequestHandler();

            try
            {
                var token = Data.App.Database.GetItems<TokenItem>();
                if (token.Count > 0)
                {
                    FormData formData = new FormData()
                    {
                        method = "GET",
                        path = "/accounts/user/", // account_user
                        credentials = true,
                        data = null
                    };

                    ct.ThrowIfCancellationRequested();

                    int index = requestHandler.addNewToken();
                    ResponseItem response = await requestHandler.call(formData, index);

                    ct.ThrowIfCancellationRequested();

                    if (response != null && response.content != "" && !response.isError)
                    {
                        UserItem new_user = JsonConvert.DeserializeObject<UserItem>(response.content);
                        new_user.registered = false;
                        Data.App.Database.SaveItem(new_user);
                        return new_user;
                    }
                }
            }
            catch (System.OperationCanceledException ex)
            {
                requestHandler.CancelAllTokens();
            }

            return null;
        }

        public static List<ScoreItem> get_score(ResponseItem response)
        {
            if (response.content != "" && !response.isError)
            {
                return JsonConvert.DeserializeObject<List<ScoreItem>>(response.content);
            }
            return null;
        }
    }
}
