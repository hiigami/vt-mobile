﻿using System;
using System.Threading.Tasks;
using Android.Locations;
using Android.OS;
using Android.Util;

namespace fluxon.API
{
    public class MyLocation : Location
    {
        float _VerticalAccuracyMeters = 50F;
        public MyLocation(Location l) : base(l)
        {
            this._VerticalAccuracyMeters = l.Accuracy;
        }

        public override float VerticalAccuracyMeters
        {
            get
            {
                try
                {
                    return base.VerticalAccuracyMeters;
                }
                catch (Exception ex)
                {
                    if (this.Accuracy < _VerticalAccuracyMeters)
                    {
                        return this.Accuracy;
                    }
                    return _VerticalAccuracyMeters;
                }
            }
            set => _VerticalAccuracyMeters = value;
        }

    }

    public class GeoHandler : Java.Lang.Object, ILocationListener
    {
        int _size;
        int attemps_limit;
        int location_counter;
        int delay_between_calls = 900;
        long[] times;
        Location[] _locations;

        protected LocationManager LocMgr =
            Android.App.Application.Context.GetSystemService("location") as LocationManager;

        public Criteria locationCriteria;

        public Location[] locations
        {
            get => _locations;
        }

        public event EventHandler<LocationChangedEventArgs> Completed = delegate { };
        public event EventHandler Error = delegate { };
        public event EventHandler<LocationChangedEventArgs> LocationChanged = delegate { };
        public event EventHandler<ProviderDisabledEventArgs> ProviderDisabled = delegate { };
        public event EventHandler<ProviderEnabledEventArgs> ProviderEnabled = delegate { };
        public event EventHandler<StatusChangedEventArgs> StatusChanged = delegate { };

        public GeoHandler(int size = 2, int attemps = 4)
        {
            _size = size;
            attemps_limit = 6 + (4 * attemps); // 4 = 5

            locationCriteria = new Criteria();
            locationCriteria.PowerRequirement = Power.NoRequirement;
            locationCriteria.AltitudeRequired = true;
            locationCriteria.Accuracy = Accuracy.Fine;
            locationCriteria.VerticalAccuracy = Accuracy.Fine;
            locationCriteria.HorizontalAccuracy = Accuracy.Fine;
            //locationCriteria.CostAllowed = true;
        }

        void down_grade_criteria(int mult)
        {
            if (locationCriteria.Accuracy == Accuracy.Fine)
            {
                locationCriteria.Accuracy = Accuracy.High;
                locationCriteria.VerticalAccuracy = Accuracy.High;
                locationCriteria.HorizontalAccuracy = Accuracy.High;
            }
            else if (locationCriteria.Accuracy == Accuracy.High)
            {
                locationCriteria.Accuracy = Accuracy.Coarse;
                locationCriteria.VerticalAccuracy = Accuracy.Coarse;
                locationCriteria.HorizontalAccuracy = Accuracy.Coarse;
            }
            else if (locationCriteria.Accuracy == Accuracy.Coarse)
            {
                locationCriteria.Accuracy = Accuracy.Coarse;
                locationCriteria.VerticalAccuracy = Accuracy.Coarse;
                locationCriteria.HorizontalAccuracy = Accuracy.Coarse;
            }

            if (mult == 14) // 2x
            {
                locationCriteria.PowerRequirement = Power.Medium;
            }
            else if (mult == 18) // 3x
            {
                locationCriteria.PowerRequirement = Power.High;
            }
            /*else if (mult == 17) // 4x
            {
                throw new Exception("GeoHandler cant find any data");
            }*/
        }

        async void down_grade(int mult)
        {
            Log.Debug("GeoHandler.down_grade", (delay_between_calls * mult).ToString());
            await Task.Delay(delay_between_calls * mult);

            LocMgr.RemoveUpdates(this);

            down_grade_criteria(mult);

            try
            {
                string locationProvider = LocMgr.GetBestProvider(locationCriteria, true);
                if ((_size - 1) >= location_counter && mult <= attemps_limit)
                {
                    LocMgr.RequestLocationUpdates(locationProvider, delay_between_calls, 0, this);
                    down_grade((mult + 4));
                } else
                {
                    this.Error(this, new EventArgs());
                }
            }
            catch (Exception ex)
            {
                Log.Debug("GeoHandler.down_grade", ex.Message);
            }
        }

        public bool is_enabled()
        {
            var providers = LocMgr.GetProviders(locationCriteria, true);
            return providers.Count > 0;
        }

        void _reset()
        {
            location_counter = 0;
            times = new long[_size];
            _locations = new Location[_size];
        }

        public void Start(int delay = -1)
        {
            if (delay > 0)
            {
                delay_between_calls = delay;
            }

            _reset();

            string locationProvider = LocMgr.GetBestProvider(locationCriteria, true);
            LocMgr.RequestLocationUpdates(locationProvider, delay_between_calls, 0, this);
            down_grade(6);
        }

        #region Helpers

        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        public static double distance(double lat1, double lon1, double lat2, double lon2)
        {
            // https://en.wikipedia.org/wiki/Haversine_formula
            double R = 6373; // Earth radius in KM.
            double x1 = deg2rad(lat2 - lat1);
            double x2 = deg2rad(lon2 - lon1);
            double a = Math.Sin(x1 / 2) * Math.Sin(x1 / 2) +
                           Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) *
                           Math.Sin(x2 / 2) * Math.Sin(x2 / 2);
            return R * 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        }

        float GetVerticalAccuracyMeters(Location l)
        {
            MyLocation myLocation = new MyLocation(l);
            return myLocation.VerticalAccuracyMeters;
        }

        public MyLocation Midpoint(Location l1, Location l2)
        {
            MyLocation l = new MyLocation(l1);
            l.Latitude = (l1.Latitude + l2.Latitude) / 2D;
            l.Longitude = (l1.Longitude + l2.Longitude) / 2D;
            l.Altitude = (l1.Altitude + l2.Altitude) / 2D;
            l.Accuracy = l1.Accuracy > l2.Accuracy
                ? l1.Accuracy
                : l2.Accuracy;
            float va1 = GetVerticalAccuracyMeters(l1);
            float va2 = GetVerticalAccuracyMeters(l2);
            l.VerticalAccuracyMeters = va1 > va2
                ? va1
                : va2;

            return l;
        }

        #endregion

        #region ILocationListener implementation

        public void OnLocationChanged(Android.Locations.Location location)
        {
            this.LocationChanged(this, new LocationChangedEventArgs(location));

            if (_size > location_counter)
            {
                _locations[location_counter] = location;
                times[location_counter] = location.Time;
                location_counter++;
            }

            if (location_counter > (_size - 1))
            {
                LocMgr.RemoveUpdates(this);
                this.Completed(this, new LocationChangedEventArgs(location));
            }
        }

        public void OnProviderDisabled(string provider)
        {
            this.ProviderDisabled(this, new ProviderDisabledEventArgs(provider));
        }

        public void OnProviderEnabled(string provider)
        {
            this.ProviderEnabled(this, new ProviderEnabledEventArgs(provider));
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            this.StatusChanged(this, new StatusChangedEventArgs(provider, status, extras));
        }

        #endregion
    }
}
