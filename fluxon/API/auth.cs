﻿using System;
using System.Threading;
using System.Text.RegularExpressions;
using RestSharp;
using fluxon.Models;
using Newtonsoft.Json;

namespace fluxon.API
{
    public class Auth
    {
        public static bool isAuthorized = true;

        readonly string login_path = "/o/token/";
        readonly string cliente_id = "81dOUjbz0rqUK62Fgs9RlBhAX1ulziamNkhXA1Qw";

        public Auth()
        {
        }

        #region No requests

        public async System.Threading.Tasks.Task<RestRequest> get_credentials(RestRequest request,
                                                                              bool credentials,
                                                                              CancellationToken ct)
        {
            if (credentials)
            {
                var token = Data.App.Database.GetItems<TokenItem>();
                if (token.Count > 0 && token[0].limit <= DateTime.Now)
                {
                    bool refresh = await refresh_token(token[0], ct);
                    if (refresh)
                    {
                        token = Data.App.Database.GetItems<TokenItem>();
                    } // sacar de la aplicacion a pantalla de login
                }
                request.AddHeader("Authorization",
                                  string.Format("{0} {1}",
                                                token[0].token_type,
                                                token[0].access_token));
            }

            return request;
        }

        public void log_out()
        {
            Data.App.Database.raw("DELETE FROM TokenItem", new object[] { });
            Data.App.Database.raw("DELETE FROM UserItem", new object[] { });
            Data.App.Database.raw("DELETE FROM ScoreItem", new object[] { });
            Data.App.Database.raw("DELETE FROM TimerItem", new object[] { });
        }

        public bool logIn(ResponseItem response, CancellationToken ct)
        {
            if (response.statusCode == System.Net.HttpStatusCode.OK)
            {
                TokenItem token = JsonConvert.DeserializeObject<TokenItem>(response.content);
                var token_saved = Data.App.Database.GetItems<TokenItem>();
                if (token_saved.Count > 0)
                {
                    token.ID = token_saved[0].ID;
                }

                DateTime limit = DateTime.Now;
                token.limit = limit.AddSeconds(Convert.ToDouble(token.expires_in));
                Data.App.Database.SaveItem(token);
                System.Threading.Tasks.Task task = System.Threading.Tasks.Task.Run(async () => await core.get_user(ct));

                try
                {
                    task.Wait();
                }
                catch (Exception ex)
                {
                    //Log.Error("API.auth.logIn", ex.Message, ex);
                }

                return true;
            }

            return false;
        }

        public bool logIn(string response, CancellationToken ct)
        {
            var token_saved = Data.App.Database.GetItems<TokenItem>();
            TokenItem tokenItem = new TokenItem();
            bool isValid = false;
            if (token_saved.Count > 0)
            {
                tokenItem.ID = token_saved[0].ID;
            }
            Match match = Regex.Match(response, @"access_token\\"":\\""(.*?)\\""",
                                      RegexOptions.IgnoreCase);
            if (match.Success)
            {
                tokenItem.access_token = match.Groups[1].Value;
                isValid = true;

                match = Regex.Match(response, @"expires_in\\"":(.*?),",
                                    RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    tokenItem.expires_in = int.Parse(match.Groups[1].Value);
                    DateTime limit = DateTime.Now;
                    tokenItem.limit = limit.AddSeconds(Convert.ToDouble(tokenItem.expires_in));
                }
                match = Regex.Match(response, @"token_type\\"":\\""(.*?)\\""",
                                    RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    tokenItem.token_type = match.Groups[1].Value;
                }
                match = Regex.Match(response, @"refresh_token\\"":\\""(.*?)\\""",
                                    RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    tokenItem.refresh_token = match.Groups[1].Value;
                }
                match = Regex.Match(response, @"scope\\"":\\""(.*?)\\""",
                                    RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    tokenItem.scope = match.Groups[1].Value;
                    Data.App.Database.SaveItem(tokenItem);
                    System.Threading.Tasks.Task task = System.Threading.Tasks.Task.Run(async () => await core.get_user(ct));
                    try
                    {
                        task.Wait();
                    }
                    catch (Exception ex)
                    {
                        //Log.Error("API.auth.logIn", ex.Message, ex);
                    }
                }
            }
            return isValid;
        }

        public bool signIn(string response)
        {
            Match match = Regex.Match(response.Replace(" ", String.Empty), @"username",
                                      RegexOptions.IgnorePatternWhitespace);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        #endregion

        public async System.Threading.Tasks.Task<bool> log_in(string username,
                                                              string password,
                                                              CancellationToken ct)
        {
            FormData formData = new FormData()
            {
                method = "POST",
                path = login_path,
                credentials = false,
                data = new string[,]{
                { "grant_type", "password" },
                { "username", username },
                { "password", password },
                { "client_id", cliente_id }
                }
            };

            ct.ThrowIfCancellationRequested();

            RequestHandler requestHandler = new RequestHandler();
            int index = requestHandler.addNewToken();
            ResponseItem response = await requestHandler.call(formData, index);

            ct.ThrowIfCancellationRequested();

            return logIn(response, ct);
        }

        public async System.Threading.Tasks.Task<bool> refresh_token(TokenItem token,
                                                                     CancellationToken ct)
        {
            var user = Data.App.Database.GetItems<UserItem>();
            if (user.Count > 0)
            {
                FormData formData = new FormData()
                {
                    method = "POST",
                    path = login_path,
                    credentials = false,
                    data = new string[,]{
                        { "grant_type", "refresh_token" },
                        { "username", user[0].username },
                        { "refresh_token", token.refresh_token },
                        { "client_id", cliente_id },
                        { "token type", token.token_type }
                    }
                };

                ct.ThrowIfCancellationRequested();

                RequestHandler requestHandler = new RequestHandler();
                int index = requestHandler.addNewToken();
                ResponseItem response = await requestHandler.call(formData, index);

                ct.ThrowIfCancellationRequested();

                if (response != null && response.statusCode == System.Net.HttpStatusCode.OK)
                {
                    return logIn(response, ct);
                }
            }
            return false;
        }
    }
}
