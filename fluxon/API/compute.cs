﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;
using fluxon.Models;
using Android.Util;
using System.Threading.Tasks;

namespace fluxon.API
{
    public class Compute
    {
        readonly string TAG = typeof(Compute).FullName;
        readonly string compute_path = "/vt/compute/";

        public Compute()
        {
        }

        public async Task<ResponseItem> add(Decimal quote,
                                            string barcode,
                                            CancellationToken ct,
                                            MyLocation location,
                                            JObject traits = null)
        {
            JObject _data = new JObject();
            RequestHandler requestHandler = new RequestHandler();

            try
            {
                ct.ThrowIfCancellationRequested();

                _data["location"] = new JObject();
                _data["location"]["lat"] = location.Latitude;
                _data["location"]["lng"] = location.Longitude;
                _data["location"]["prec_radius"] = location.Accuracy;
                _data["location"]["altitude"] = location.Altitude;
                _data["location"]["altitude_accuracy"] = location.VerticalAccuracyMeters;
                _data["quote"] = quote;
                _data["barcode"] = barcode;

                if (traits != null)
                    _data.Merge(traits);

                JsonData formData = new JsonData()
                {
                    method = "POST",
                    path = compute_path,
                    credentials = true,
                    data = _data
                };

                int index = requestHandler.addNewToken();

                ct.ThrowIfCancellationRequested();

                ResponseItem response = await requestHandler.call(formData, index);

                ct.ThrowIfCancellationRequested();

                return response;

            }
            catch (OperationCanceledException ex)
            {
                Log.Debug(TAG, ex.Message, ex);
            }
            catch (Exception ex)
            {
                Log.Error(TAG, ex.Message, ex);
            }
            return null;
        }
    }
}
