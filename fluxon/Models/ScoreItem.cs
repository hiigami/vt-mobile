﻿using SQLite;

namespace fluxon.Models
{
	public class ScoreItem
	{
		public ScoreItem()
		{
		}
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public decimal score_update { get; set;}
		public int change { get; set; }
		public string barcode { get; set; }
	}
}
