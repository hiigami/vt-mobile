﻿using System;
using SQLite;

namespace fluxon.Models
{
	public class TokenItem
	{
		public TokenItem()
		{
		}
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public string access_token { get; set; }
		public string refresh_token { get; set; }
		public string token_type { get; set; }
		public int expires_in { get; set; }
		public string scope { get; set; }
		public DateTime limit { get; set; }
	}
}
