﻿using System;
using SQLite;

namespace fluxon.Models
{
	public class TimerItem
	{
		public TimerItem()
		{
		}
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public DateTime created { get; set; }
		public Int16 limit_minutes { get; set; }
		public string barcode { get; set; }
	}
}