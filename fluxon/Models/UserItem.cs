﻿using SQLite;

namespace fluxon.Models
{
    public class UserItem
    {
        public UserItem()
        {
        }
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string username { get; set; }
        public string email { get; set; }
        public bool registered { get; set; }
    }
}