﻿using System.Net;
namespace fluxon.Models
{
    public class ResponseItem
    {
        public ResponseItem(HttpStatusCode statusCode, string content, bool error = false, long payload = 0)
        {
            this.content = content;
            this.statusCode = statusCode;
            this.isError = error;
            this.data_size = payload;
        }

        public HttpStatusCode statusCode { get; set; }
        public string content { get; set; }
        public bool isError { get; set; }
        public long data_size { get; set; }
    }
}
