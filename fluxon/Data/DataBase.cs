﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SQLite;
using fluxon.Models;

namespace fluxon.Data
{
	public class DataBase
	{
		static object locker = new object();

		SQLiteConnection database;

		string DatabasePath

		{
			get

			{
				var sqliteFilename = "fluxon.db3";
#if __IOS__
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
                var path = Path.Combine(libraryPath, sqliteFilename);
#else
#if __ANDROID__
				string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
#else
				// WinPhone
				var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);;
#endif
#endif
				return path;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Tasky.DL.TaskDatabase"/> TaskDatabase. 
		/// if the database doesn't exist, it will create the database and all the tables.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		public DataBase()
		{
			database = new SQLiteConnection(DatabasePath);
			// create the tables
			database.CreateTable<ScoreItem>();
			database.CreateTable<TokenItem>();
			database.CreateTable<UserItem>();
			database.CreateTable<TimerItem>();
		}
		public List<T> GetItems<T>() where T: class, new()
		{
			lock (locker)
			{
				return (from i in database.Table<T>() select i).ToList();
			}
		}
		public int SaveItem<T>(T item)
		{
			lock (locker)
			{
				if (item.GetType().GetProperty("ID") != null && (int)item.GetType().GetProperty("ID").GetValue(item, null) != 0)
				{
					return database.Update(item);
				}
				return database.Insert(item);
			}
		}
		public int DeleteItem<T>(int id)
		{
			lock (locker)
			{
				return database.Delete<T>(id);
			}
		}
        public int raw(string query, params object[] args)
        {
            lock (locker)
            {
                return database.Execute(query, args);
            }
        }
	}
}
