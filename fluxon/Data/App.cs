﻿using System;
using System.Collections.Generic;
using System.Text;

namespace fluxon.Data
{
	public class App
	{
		static DataBase database = new DataBase();

		public static DataBase Database
		{
			get
			{
				database = database ?? new DataBase();
				return database;
			}
		}
	}
}
