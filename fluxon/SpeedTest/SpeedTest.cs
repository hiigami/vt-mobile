﻿using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

using Android.OS;

using RestSharp;

using fluxon.Models;

namespace fluxon.SpeedTest
{
    public class NewRequest
    {
        public NewRequest()
        {
        }
        public NewRequest(string url, int bump)
        {
            this.url = url;
            this.bump = bump;
        }
        public string url { get; set; }
        public int bump { get; set; }
        public byte[] data { get; set; }
    }
    public class BestServer
    {
        public BestServer()
        {
        }
        public BestServer(double latency, XmlElement server)
        {
            this.latency = latency;
            this.server = server;
        }
        public double latency { get; set; }
        public XmlElement server { get; set; }
    }
    struct Sizes
    {
        public int[] upload;
        public int[] download;
    };
    struct UploadDownload
    {
        public int upload;
        public int download;
    };
    struct Config
    {
        public XmlNode client;
        public List<int> ignore_servers;
        public Sizes sizes;
        public UploadDownload counts;
        public UploadDownload threads;
        public UploadDownload length;
        public int upload_max;
        public double lat;
        public double lng;
    };

    public class SpeedTest
    {
        readonly string _version = "1.0.7";
        readonly int timeout_milliseconds_download = 10000; // 10 sec
        readonly int timeout_milliseconds_upload = 20000; // 20 sec
        int[] _upload_sizes = new int[] { 32768, 65536, 131072, 262144, 524288, 1048576, 7340032 };
        int[] _download_sizes = new int[] { 350, 500, 750, 1000, 1500, 2000, 2500, 3000, 3500, 4000 };
        Config _config;
        public double download_result = 0;
        public double upload_result = 0;
        public double ping_result = 0;
        //closest
        BestServer best = new BestServer();
        Dictionary<double, XmlElement> servers = new Dictionary<double, XmlElement>();
        Dictionary<string, string> urls = new Dictionary<string, string>()
        {
            {"config", "http://www.speedtest.net/speedtest-config.php"},
            {
                "servers", "http://www.speedtest.net/speedtest-servers-static.php," +
                    "http://c.speedtest.net/speedtest-servers-static.php," +
                    "http://www.speedtest.net/speedtest-servers.php," +
                    "http://c.speedtest.net/speedtest-servers.php"
            }
        };

        public SpeedTest()
        {
        }

        string buildUrl(string url)
        {
            var uriBuilder = new UriBuilder(url);
            string new_url = uriBuilder.Scheme + "://" + uriBuilder.Host + ":" + uriBuilder.Port.ToString();
            int count = (uriBuilder.Uri.Segments.Length - 1);

            for (int i = 0; i < count; i++)
            {
                new_url += uriBuilder.Uri.Segments[i];
            }
            return new_url;
        }

        byte[] BuildData(double length)
        {
            string c = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int multiplier = int.Parse(Math.Round(length / 36D).ToString());
            int max_length = int.Parse(length.ToString()) - 9;
            StringBuilder sb = new StringBuilder();
            //sb.Append("content1=");
            for (int i = 0; i < multiplier; i++)
            {
                sb.Append(c);
            }
            string somestring = sb.ToString().Substring(0, max_length);
            return Encoding.ASCII.GetBytes(somestring);
        }

        string build_user_agent()
        {
            return String.Format("Mozilla/5.0 (Linux; U; Android {0}; {1}; en-us) (KHTML, like Gecko) speedtest-cli/{2}",
                                 Build.VERSION.Release,
                                 Android.OS.Build.Model,
                                 _version
                                );
        }

        Method get_method(string name)
        {
            if (name == "POST")
                return Method.POST;
            return Method.GET;
        }

        async Task<ResponseItem> buildRequest(string method_name, string url, byte[] data, string[,] headers, string bump = "")
        {
            Method method = get_method(method_name);
            var uriBuilder = new UriBuilder(url);
            Int64 timeOut;
            Int64.TryParse((DateTime.Now.TimeOfDay.Ticks * 1000).ToString(), out timeOut);
            int timeout_milliseconds = data == null ? timeout_milliseconds_download : timeout_milliseconds_upload;
            uriBuilder.Query += string.Format("x={0}.{1}",
                                              timeOut,
                                              bump);
            var client = new RestClient
            {
                Timeout = timeout_milliseconds,
                BaseUrl = new Uri(uriBuilder.Scheme + "://" + uriBuilder.Host + ":" + uriBuilder.Port.ToString()),
                UserAgent = build_user_agent()
            };

            var request = new RestRequest(uriBuilder.Path + uriBuilder.Query, method);

            request.AddHeader("Cache-Control", "no-cache");
            if (headers != null)
            {
                for (int i = 0; i < headers.GetLength(0); i++)
                {
                    request.AddHeader(headers[i, 0], headers[i, 1]);
                }
            }

            if (data != null)
            {
                request.AddFileBytes("content1", data, "f.txt");
            }

            var taskCompletionSource = new TaskCompletionSource<ResponseItem>();

            client.ExecuteAsync(request, response =>
            {
                if (response.ErrorException != null)
                {
                    taskCompletionSource.SetResult(new ResponseItem(response.StatusCode,
                                                                    response.ErrorMessage,
                                                                       true));
                }
                else if (response.ResponseStatus == ResponseStatus.Completed)
                {
                    try
                    {
                        long payload = 0;
                        if (response.Request.Files.Count > 0)
                        {
                            payload = response.Request.Files[0].ContentLength;
                        }
                        taskCompletionSource.SetResult(new ResponseItem(response.StatusCode, response.Content, false, payload));
                    }
                    catch (Exception ex)
                    {
                        taskCompletionSource.SetResult(new ResponseItem(response.StatusCode, ex.Message, true));
                    }
                }
                else
                {
                    taskCompletionSource.SetResult(new ResponseItem(response.StatusCode, "", true));
                }
            });
            return await taskCompletionSource.Task;
        }

        public async Task<int> GetConfig()
        {
            var request = await buildRequest("POST", urls["config"], null, null);

            if (request.isError)
            {
                return 1;
            }

            try
            {
                XmlDocument config = new XmlDocument();
                byte[] byteArray = Encoding.ASCII.GetBytes(request.content);
                MemoryStream stream = new MemoryStream(byteArray);
                config.Load(stream);

                XmlNode server_config = config.DocumentElement.SelectSingleNode("server-config");
                XmlElement server_config_el = (XmlElement)server_config;
                string ignoreids_txt = server_config_el.GetAttributeNode("ignoreids").InnerXml;
                _config.ignore_servers = ignoreids_txt.Split(',').Select(int.Parse).ToList();
                _config.threads.download = int.Parse(server_config_el.GetAttributeNode("threadcount").InnerXml) * 2;


                XmlNode download_node = config.DocumentElement.SelectSingleNode("download");
                XmlElement download_el = (XmlElement)download_node;
                _config.counts.download = int.Parse(download_el.GetAttributeNode("threadsperurl").InnerXml);
                _config.length.download = int.Parse(download_el.GetAttributeNode("testlength").InnerXml);

                XmlNode upload = config.DocumentElement.SelectSingleNode("upload");
                XmlElement upload_el = (XmlElement)upload;

                int ratio = int.Parse(upload_el.GetAttributeNode("ratio").InnerXml);

                int upload_max = int.Parse(upload_el.GetAttributeNode("maxchunkcount").InnerXml);
                _config.threads.upload = int.Parse(upload_el.GetAttributeNode("threads").InnerXml);
                _config.length.upload = int.Parse(upload_el.GetAttributeNode("testlength").InnerXml);

                _config.client = config.DocumentElement.SelectSingleNode("client");
                XmlElement client_el = (XmlElement)_config.client;
                _config.lat = double.Parse(client_el.GetAttributeNode("lat").InnerXml);
                _config.lng = double.Parse(client_el.GetAttributeNode("lon").InnerXml);

                int take = ratio - 1;
                _config.sizes.upload = _upload_sizes.Skip(take).Take((_upload_sizes.Length - take)).ToArray();
                _config.sizes.download = _download_sizes;
                _config.counts.upload = int.Parse(Math.Ceiling(double.Parse((upload_max / _config.sizes.upload.Length).ToString())).ToString());
                _config.upload_max = _config.counts.upload * _config.sizes.upload.Length;

                return 0;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }

        public async Task<int> GetServers()
        {
            string[] servers_url = urls["servers"].Split(',');

            foreach (string url in servers_url)
            {
                string new_url = string.Format("{0}?threads={1}", url, _config.threads.download);

                var request = await buildRequest("POST", new_url, null, null);
                if (request.isError)
                {
                    continue;
                }
                try
                {
                    XmlDocument config = new XmlDocument();
                    byte[] byteArray = Encoding.ASCII.GetBytes(request.content);
                    MemoryStream stream = new MemoryStream(byteArray);
                    config.Load(stream);

                    XmlNodeList servers_list = config.GetElementsByTagName("server");

                    foreach (var server in servers_list)
                    {
                        XmlElement el = (XmlElement)server;
                        int id = int.Parse(el.GetAttributeNode("id").Value);
                        if (_config.ignore_servers.Contains(id))
                        {
                            continue;
                        }
                        double lat = double.Parse(el.GetAttributeNode("lat").Value);
                        double lng = double.Parse(el.GetAttributeNode("lon").Value);
                        double _distance = fluxon.API.GeoHandler.distance(_config.lat, _config.lng, lat, lng);
                        el.SetAttribute("d", _distance.ToString());
                        if (!servers.ContainsKey(_distance))
                        {
                            servers.Add(_distance, el);
                        }
                    }
                }
                catch (Exception ex) { }
            }

            return 0;
        }

        Dictionary<double, XmlElement> get_closest_servers(int limit = 5)
        {
            Dictionary<double, XmlElement> closest_servers = new Dictionary<double, XmlElement>();
            var list = servers.Keys.ToList();
            list.Sort();
            foreach (double id in list)
            {
                closest_servers.Add(id, servers[id]);
                if (closest_servers.Count == limit)
                {
                    break;
                }
            }
            servers.Clear();
            return closest_servers;
        }

        public async Task<int> GetBestServer()
        {
            Dictionary<double, XmlElement> closest_servers = get_closest_servers();
            Dictionary<double, XmlElement> results = new Dictionary<double, XmlElement>();

            foreach (var pair in closest_servers)
            {
                List<double> total = new List<double>();

                string url = buildUrl(pair.Value.GetAttributeNode("url").InnerXml);
                url += "latency.txt";

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                for (int i = 0; i < 3; i++)
                {
                    stopwatch.Restart();
                    try
                    {
                        var request = await buildRequest("GET", url, null, null);
                        stopwatch.Stop();
                        if (request.content.Contains("test=test"))
                        {
                            total.Add(double.Parse(stopwatch.ElapsedMilliseconds.ToString()) / 1000D);
                        }
                        else
                        {
                            total.Add(3600);
                        }
                    }
                    catch (Exception ex)
                    {
                        total.Add(3600);
                    }
                }
                double avg = Math.Round((total.Sum() / 6D) * 1000D, 3);

                if (!results.ContainsKey(avg))
                {
                    results.Add(avg, pair.Value);
                }
            }

            var list = results.Keys.ToList();
            list.Sort();

            ping_result = list[0];
            best.latency = ping_result;
            best.server = results[ping_result];

            list.Clear();
            closest_servers.Clear();
            results.Clear();

            return 0;
        }

        async Task<ResponseItem> Producer(NewRequest request)
        {
            return await buildRequest("POST", request.url, request.data, null, request.bump.ToString());
        }

        public async Task<double> Download()
        {
            List<NewRequest> _urls = new List<NewRequest>();
            string server_url = buildUrl(best.server.GetAttributeNode("url").InnerXml);

            foreach (var _size in _config.sizes.download)
            {
                for (int i = _config.counts.download; i > 0; i--)
                {
                    string _u = string.Format("{0}random{1}x{2}.jpg",
                                              server_url,
                                              _size,
                                              _size);
                    _urls.Add(new NewRequest(_u, i));
                }
            }

            int request_count = _urls.Count;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            ResponseItem[] consumer = await Task.WhenAll(_urls.Select(x => Producer(x)));

            stopwatch.Stop();
            double total_time = double.Parse(stopwatch.ElapsedMilliseconds.ToString()) / 1000D;

            long total_bytes = 0;
            foreach (ResponseItem item in consumer)
            {
                if (item.statusCode == System.Net.HttpStatusCode.OK)
                {
                    total_bytes += item.content.Length;
                }
            }

            download_result = (total_bytes / total_time) * 8D;

            if (download_result > 100000D)
            {
                _config.threads.upload = 8;
            }

            _urls.Clear();
            consumer = new ResponseItem[0];
            return download_result;
        }

        public async Task<double> Upload()
        {
            List<NewRequest> _urls = new List<NewRequest>();
            string server_url = best.server.GetAttributeNode("url").InnerXml;

            foreach (int _size in _config.sizes.upload)
            {
                for (int i = _config.counts.upload; i > 0; i--)
                {
                    NewRequest el = new NewRequest(server_url, i);
                    el.data = BuildData(_size);
                    _urls.Add(el);
                }
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            ResponseItem[] consumer = await Task.WhenAll(_urls.Select(x => Producer(x)));

            stopwatch.Stop();
            double total_time = double.Parse(stopwatch.ElapsedMilliseconds.ToString()) / 1000D;

            long total_bytes = 0;
            foreach (ResponseItem item in consumer)
            {
                if (item.statusCode == System.Net.HttpStatusCode.OK)
                {
                    total_bytes += item.data_size;
                }
            }

            upload_result = (total_bytes / total_time) * 8D;

            _urls.Clear();
            consumer = new ResponseItem[0];

            return upload_result;
        }
    }
}
